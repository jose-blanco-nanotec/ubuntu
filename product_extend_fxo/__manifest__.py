{
    'name': 'Producto',
    'version': '11.0.1.0.0',
    'author': 'Flexxoone',
    'license': 'AGPL-3',
    'summary': 'Nuevos campos en product_template',
    'category': 'Product',
    'depends': ['product', 'sale'],
    'description': """
                Agrega nuevos campos en producto_template  (Textil)
                - Modelo, Estilo, Tela, Producto
                - Concatena en campo name
    """,

    'data': [
        'views/product_view.xml',
    ],
    'auto_install': False,
    'installable': True,
}
