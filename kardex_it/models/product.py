# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class ProductCategory(models.Model):
    _inherit = "product.category"
    einvoice_05 = fields.Many2one('einvoice.catalog.05', 'Codigo de existencia')


class product_template(models.Model):
	_inherit = 'product.template'
	unidad_kardex = fields.Many2one('product.uom',string="Unidad de Producto Kardex")