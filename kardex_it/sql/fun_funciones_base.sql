-- FUNCTION: public.getnumber(character varying)

-- DROP FUNCTION public.getnumber(character varying);

CREATE OR REPLACE FUNCTION public.getnumber(
	number character varying)
    RETURNS character varying
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
AS $BODY$
    DECLARE
    number1 ALIAS FOR $1;
    res varchar;
    BEGIN
       select substring(number1,position('-' in number1)+1) into res;
       return res;
    END;$BODY$;

CREATE OR REPLACE FUNCTION public.getserial("number" character varying)
  RETURNS character varying AS
$BODY$
DECLARE
number1 ALIAS FOR $1;
res varchar;
BEGIN
   select substring(number1,0,position('-' in number1)) into res;
   return res;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
/**********/
CREATE OR REPLACE FUNCTION public.fecha_num(date)
  RETURNS integer AS
$BODY$
    SELECT to_char($1, 'YYYYMMDD')::integer;
$BODY$
  LANGUAGE sql VOLATILE
  COST 100;
/**********/
-- FUNCTION: public.getperiod(timestamp without time zone, boolean)

-- DROP FUNCTION public.getperiod(timestamp without time zone, boolean);

CREATE OR REPLACE FUNCTION public.getperiod(
	date_picking timestamp without time zone,
	special boolean)
    RETURNS character varying
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
AS $BODY$
DECLARE
date_picking1 ALIAS FOR $1;
res varchar;
isspecial alias for $2;
BEGIN
  select account_period.name into res from account_period
  where date_start<=date_picking1 and date_stop>=date_picking1 and account_period.special=isspecial;
   return res;
END;$BODY$;


/*********/
CREATE OR REPLACE FUNCTION public.getperiod(
    move_id integer,
    date_picking date,
    special boolean)
    RETURNS character varying
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
AS $BODY$
DECLARE
move_id1 ALIAS FOR $1;
date_picking1 ALIAS FOR $2;
res varchar;
isspecial alias for special;
BEGIN
    IF move_id1 !=0 THEN
  select account_period.name into res from account_move
  inner join account_period on account_period.date_start <= account_move.date and account_period.date_stop >= account_move.date  and account_period.special = account_move.fecha_specia
  where account_move.id=move_id1;
    ELSE
  select account_period.name into res from account_period
  where date_start<=date_picking1 and date_stop>=date_picking1 and account_period.special=isspecial;
   END IF;
   return res;
END;$BODY$;

/********/
CREATE OR REPLACE FUNCTION public.getperiod(
    date_picking timestamp without time zone,
    special boolean)
    RETURNS character varying
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
AS $BODY$
DECLARE
date_picking1 ALIAS FOR $1;
res varchar;
isspecial alias for $2;
BEGIN
  select account_period.name into res from account_period
  where date_start<=date_picking1 and date_stop>=date_picking1 and account_period.special=isspecial;
   return res;
END;$BODY$;

/********************/
CREATE OR REPLACE FUNCTION public.periodo_num(
    character varying)
    RETURNS integer
    LANGUAGE 'sql'

    COST 100
    VOLATILE
AS $BODY$
    SELECT CASE WHEN substring($1,1,19) = 'Periodo de apertura' THEN (substring($1,21,4) || '00' )::integer ELSE
    (substring( $1,4,4) || substring($1 , 1,2)  )::integer END ;
$BODY$;
/****************/
CREATE OR REPLACE FUNCTION public.periodo_string(
    numero integer)
    RETURNS character varying
    LANGUAGE 'sql'

    COST 100
    VOLATILE
AS $BODY$
    SELECT CASE WHEN substring(numero::varchar,5,2) = '00' THEN 'Periodo de apertura ' || substring(numero::varchar,1,4) ELSE
    (substring(numero::varchar,5,2) || '/' ||substring(numero::varchar,1,4) )::varchar END;
$BODY$;

/****************/
CREATE OR REPLACE FUNCTION public.fecha_num(
    date)
    RETURNS integer
    LANGUAGE 'sql'

    COST 100
    VOLATILE
AS $BODY$
    SELECT to_char($1, 'YYYYMMDD')::integer;
$BODY$;

