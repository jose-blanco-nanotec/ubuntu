-- View: vst_kardex_fisico_proc_2

-- DROP VIEW vst_kardex_fisico_proc_2;

CREATE OR REPLACE VIEW vst_kardex_fisico_proc_2 AS
 SELECT vst_kardex_fis_1.id,
    vst_kardex_fis_1.origen,
    vst_kardex_fis_1.destino,
    vst_kardex_fis_1.serial,
    vst_kardex_fis_1.nro,
    vst_kardex_fis_1.cantidad AS ingreso,
    0::numeric AS salida,
    0::numeric AS saldof,
    vst_kardex_fis_1.producto,
    vst_kardex_fis_1.fecha,
    vst_kardex_fis_1.id_origen,
    vst_kardex_fis_1.id_destino,
    vst_kardex_fis_1.product_id,
    vst_kardex_fis_1.id_destino AS location_id,
    vst_kardex_fis_1.destino AS almacen,
    vst_kardex_fis_1.categoria,
    vst_kardex_fis_1.name,
    'in'::text AS type,
    'ingreso'::text AS esingreso,
    vst_kardex_fis_1.default_code,
    vst_kardex_fis_1.unidad,
        CASE
            WHEN ai.id IS NULL AND vst_kardex_fis_1.price_unit > 0::double precision AND vst_kardex_fis_1.id IS NULL THEN vst_kardex_fis_1.price_unit
            ELSE
            CASE
                WHEN ipx.value_text = 'specific'::text THEN vst_kardex_fis_1.price_unit
                ELSE
                CASE
                    WHEN btrim(vst_kardex_fis_1.type_doc::text) = '07'::text THEN vst_kardex_fis_1.price_unit * vst_kardex_fis_1.cantidad::double precision
                    ELSE vst_kardex_fis_1.price_unit * vst_kardex_fis_1.cantidad::double precision
                END
            END
        END AS debit,
        CASE
            WHEN ai.id IS NULL AND vst_kardex_fis_1.price_unit < 0::double precision AND vst_kardex_fis_1.id IS NULL THEN - vst_kardex_fis_1.price_unit::numeric
            ELSE 0::numeric
        END AS credit,
    0::numeric AS saldov,
        CASE
            WHEN btrim(vst_kardex_fis_1.type_doc::text) = '07'::text THEN vst_kardex_fis_1.price_unit
            ELSE vst_kardex_fis_1.price_unit
        END AS cadquiere,
    0::numeric AS cprom,
    vst_kardex_fis_1.periodo,
    vst_kardex_fis_1.ctanalitica,
    vst_kardex_fis_1.operation_type,
    vst_kardex_fis_1.doc_type_ope,
    vst_kardex_fis_1.product_account,
    vst_kardex_fis_1.stock_doc,
    vst_kardex_fis_1.type_doc,
    vst_kardex_fis_1.numdoc_cuadre,
    vst_kardex_fis_1.nro_documento
   FROM vst_kardex_fisico_proc_1 vst_kardex_fis_1
     JOIN stock_location ON vst_kardex_fis_1.id_destino = stock_location.id
     JOIN product_product pp ON pp.id = vst_kardex_fis_1.product_id
     JOIN product_template pt ON pt.id = pp.product_tmpl_id
     LEFT JOIN account_invoice ai ON ai.id = vst_kardex_fis_1.invoice_id
     LEFT JOIN ir_property ipx ON ipx.res_id::text = ('product.template,'::text || pt.id) AND ipx.name::text = 'cost_method'::text
  WHERE stock_location.usage::text = 'internal'::text
UNION ALL
 SELECT vst_kardex_fis_1.id,
    vst_kardex_fis_1.origen,
    vst_kardex_fis_1.destino,
    vst_kardex_fis_1.serial,
    vst_kardex_fis_1.nro,
    0::numeric AS ingreso,
    vst_kardex_fis_1.cantidad AS salida,
    0::numeric AS saldof,
    vst_kardex_fis_1.producto,
    vst_kardex_fis_1.fecha,
    vst_kardex_fis_1.id_origen,
    vst_kardex_fis_1.id_destino,
    vst_kardex_fis_1.product_id,
    vst_kardex_fis_1.id_origen AS location_id,
    vst_kardex_fis_1.origen AS almacen,
    vst_kardex_fis_1.categoria,
    vst_kardex_fis_1.name,
    'out'::text AS type,
    'salida'::text AS esingreso,
    vst_kardex_fis_1.default_code,
    vst_kardex_fis_1.unidad,
    0::numeric AS debit,
    0::numeric AS credit,
    0::numeric AS saldov,
    0::numeric AS cadquiere,
    0::numeric AS cprom,
    vst_kardex_fis_1.periodo,
    vst_kardex_fis_1.ctanalitica,
    vst_kardex_fis_1.operation_type,
    vst_kardex_fis_1.doc_type_ope,
    vst_kardex_fis_1.product_account,
    vst_kardex_fis_1.stock_doc,
    vst_kardex_fis_1.type_doc,
    vst_kardex_fis_1.numdoc_cuadre,
    vst_kardex_fis_1.nro_documento
   FROM vst_kardex_fisico_proc_1 vst_kardex_fis_1
     JOIN stock_location ON vst_kardex_fis_1.id_origen = stock_location.id
  WHERE stock_location.usage::text = 'internal'::text;

