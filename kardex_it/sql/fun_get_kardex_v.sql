CREATE OR REPLACE FUNCTION public.get_kardex_v(
    date_ini integer,
    date_end integer,
    productos integer[],
    almacenes integer[],
    OUT almacen character varying,
    OUT categoria character varying,
    OUT name_template character varying,
    OUT fecha date,
    OUT periodo character varying,
    OUT ctanalitica character varying,
    OUT serial character varying,
    OUT nro character varying,
    OUT operation_type character varying,
    OUT name character varying,
    OUT ingreso numeric,
    OUT salida numeric,
    OUT saldof numeric,
    OUT debit numeric,
    OUT credit numeric,
    OUT cadquiere numeric,
    OUT saldov numeric,
    OUT cprom numeric,
    OUT type character varying,
    OUT esingreso text,
    OUT product_id integer,
    OUT location_id integer,
    OUT doc_type_ope character varying,
    OUT ubicacion_origen integer,
    OUT ubicacion_destino integer,
    OUT stock_moveid integer,
    OUT account_invoice character varying,
    OUT product_account character varying,
    OUT default_code character varying,
    OUT unidad character varying,
    OUT mrpname character varying,
    OUT ruc character varying,
    OUT comapnyname character varying,
    OUT cod_sunat character varying,
    OUT tipoprod character varying,
    OUT coduni character varying,
    OUT metodo character varying,
    OUT cu_entrada numeric,
    OUT cu_salida numeric,
    OUT period_name character varying,
    OUT stock_doc character varying,
    OUT origen character varying,
    OUT destino character varying,
    OUT type_doc character varying,
    OUT numdoc_cuadre character varying,
    OUT doc_partner character varying,
    OUT fecha_albaran date,
    OUT pedido_compra character varying,
    OUT licitacion character varying,
    OUT doc_almac character varying,
    OUT lote character varying)
    RETURNS SETOF record
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$
DECLARE
  location integer;
  product integer;
  precprom numeric;
  h record;
  h1 record;
  h2 record;
  dr record;
  pt record;
  il record;
  loc_id integer;
  prod_id integer;
  contador integer;
  lote_idmp varchar;

BEGIN

  select res_partner.name,res_partner.nro_documento from res_company
  inner join res_partner on res_company.partner_id = res_partner.id
  into h;

  /*-- foreach product in array $3 loop*/

            loc_id = -1;
            prod_id = -1;
            lote_idmp = -1;
/*--    foreach location in array $4  loop
--      for dr in cursor_final loop*/
      saldof =0;
      saldov =0;
      cprom =0;
      cadquiere =0;
      ingreso =0;
      salida =0;
      debit =0;
      credit =0;
           contador = 2;


      for dr in
      select *,sp.name as doc_almac,sp.fecha_kardex::date as fecha_albaran, po.name as pedido_compra, pr.name as licitacion,spl.name as lote,
      ''::character varying as ruc,''::character varying as comapnyname, ''::character varying as cod_sunat,''::character varying as default_code,ipx.value_text as ipxvalue,
      ''::character varying as tipoprod ,''::character varying as coduni ,''::character varying as metodo, 0::numeric as cu_entrada , 0::numeric as cu_salida, ''::character varying as period_name
      from vst_kardex_fisico_valorado as vst_kardex_sunat
        left join stock_move sm on sm.id = vst_kardex_sunat.stock_moveid
        /*left join stock_production_lot spl on spl.id = sm.restrict_lot_id*/
        left join stock_production_lot spl on spl.id = sm.created_production_id
        left join stock_picking sp on sp.id = sm.picking_id
        left join purchase_order po on po.id = sp.po_id
        left join purchase_requisition pr on pr.id = po.requisition_id
        left join account_invoice_line ail on ail.id = vst_kardex_sunat.invoicelineid
        left join product_product pp on pp.id = vst_kardex_sunat.product_id
        left join product_template ptp on ptp.id = pp.product_tmpl_id
        LEFT JOIN ir_property ipx ON ipx.res_id::text = ('product.template,'::text || ptp.id) AND ipx.name::text = 'cost_method'::text

       where fecha_num(vst_kardex_sunat.fecha::date) between $1 and $2
      order by vst_kardex_sunat.location_id,vst_kardex_sunat.product_id,vst_kardex_sunat.fecha,vst_kardex_sunat.esingreso,vst_kardex_sunat.stock_moveid,vst_kardex_sunat.nro
        loop
        if dr.location_id = ANY ($4) and dr.product_id = ANY ($3) then
          if dr.ipxvalue = 'specific' then
                    if loc_id = dr.location_id then
              contador = 1;
              else

              loc_id = dr.location_id;
              prod_id = dr.product_id;
          /*--    foreach location in array $4  loop

          --      for dr in cursor_final loop*/
              saldof =0;
              saldov =0;
              cprom =0;
              cadquiere =0;
              ingreso =0;
              salida =0;
              debit =0;
              credit =0;
            end if;
              else


                if prod_id = dr.product_id and loc_id = dr.location_id then
                contador =1;
                else

              loc_id = dr.location_id;
              prod_id = dr.product_id;
          /*--    foreach location in array $4  loop
          --      for dr in cursor_final loop*/
                saldof =0;
                saldov =0;
                cprom =0;
                cadquiere =0;
                ingreso =0;
                salida =0;
                debit =0;
                credit =0;
                end if;
           end if;

            select '' as category_sunat_code, '' as uom_sunat_code
            from product_product
            inner join product_template on product_product.product_tmpl_id = product_template.id
            inner join product_category on product_template.categ_id = product_category.id
            inner join product_uom on product_template.uom_id = product_uom.id
            /*--left join category_product_sunat on product_category.cod_sunat = category_product_sunat.id
            --left join category_uom_sunat on product_uom.cod_sunat = category_uom_sunat.id*/
            where product_product.id = dr.product_id into h1;

                              select * from stock_location where id = dr.location_id into h2;

          /*---- esto es para las variables que estan en el crusor y pasarlas a las variables output*/

          almacen=dr.almacen;
          categoria=dr.categoria;
          name_template=dr.producto;
          fecha=dr.fecha;
          periodo=dr.periodo;
          ctanalitica=dr.ctanalitica;
          serial=dr.serial;
          nro=dr.nro;
          operation_type=dr.operation_type;
          name=dr.name;
          type=dr.type;
          esingreso=dr.esingreso;
          product_id=dr.product_id;

          location_id=dr.location_id;
          doc_type_ope=dr.doc_type_ope;
          ubicacion_origen=dr.id_origen;
          ubicacion_destino=dr.id_destino;
          stock_moveid=dr.stock_moveid;
          account_invoice=0;
          product_account=dr.product_account;
          default_code=dr.default_code;
          unidad=dr.unidad;
          mrpname='';
          stock_doc=dr.stock_doc;
          origen=dr.origen;
          destino=dr.destino;
          type_doc=dr.type_doc;
                numdoc_cuadre=dr.numdoc_cuadre;
                doc_partner=dr.nro_documento;
                lote= dr.lote;



           ruc = h.nro_documento;
           comapnyname = h.name;
           cod_sunat = '';
           default_code = dr.default_code;
           tipoprod = h1.category_sunat_code;
           coduni = h1.uom_sunat_code;
           metodo = 'Costo promedio';

           period_name = dr.period_name;

           fecha_albaran = dr.fecha_albaran;
           pedido_compra = dr.pedido_compra;
           licitacion = dr.licitacion;
           doc_almac = dr.doc_almac;

          /*--- final de proceso de variables output*/


          ingreso =coalesce(dr.ingreso,0);
          salida =coalesce(dr.salida,0);
          --if dr.serial is not null then
          debit=coalesce(dr.debit,0);
          /*
          --else
            --if dr.ubicacion_origen=8 then
              --debit =0;
            --else
              ---debit = coalesce(dr.debit,0);
            --end if;
          --end if;*/
          credit =coalesce(dr.credit,0);

          cadquiere =coalesce(dr.cadquiere,0);
          precprom = cprom;
          if cadquiere <=0::numeric then
            cadquiere=cprom;
          end if;
          if salida>0::numeric then
            credit = cadquiere * salida;
          end if;
          saldov = saldov + (debit - credit);
          saldof = saldof + (ingreso - salida);
          if saldof > 0::numeric then
            if esingreso= 'ingreso' or ingreso > 0::numeric then
              if saldof != 0 then
                cprom = saldov/saldof;
              else
                      cprom = saldov;
                 end if;
              if ingreso = 0 then
                      cadquiere = cprom;
              else
                  cadquiere =debit/ingreso;
              end if;
              /*--cprom = saldov / saldof;*/
              /*--cadquiere = debit / ingreso;*/
            else
              if salida = 0::numeric then
                if debit + credit > 0::numeric then
                  cprom = saldov / saldof;
                  cadquiere=cprom;
                end if;
              else
                credit = salida * cprom;
              end if;
            end if;
          else
            cprom = 0;
          end if;


          if saldov <= 0::numeric and saldof <= 0::numeric then
            dr.cprom = 0;
            cprom = 0;
          end if;
          /*--if cadquiere=0 then
          --  if trim(dr.operation_type) != '05' and trim(dr.operation_type) != '' and dr.operation_type is not null then
          --    cadquiere=precprom;
          --    debit = ingreso*cadquiere;
          --    credit=salida*cadquiere;
          --  end if;
          --end if;*/
          dr.debit = round(debit,2);
          dr.credit = round(credit,2);
          dr.cprom = round(cprom,8);
          dr.cadquiere = round(cadquiere,8);
          dr.credit = round(credit,2);
          dr.saldof = round(saldof,2);
          dr.saldov = round(saldov,8);
          if ingreso>0 then
            cu_entrada =debit/ingreso;
          else
            cu_entrada =debit;
          end if;

          if salida>0 then
            cu_salida =credit/salida;
          else
          cu_salida =credit;
          end if;

          RETURN NEXT;
        end if;
  end loop;
END
$BODY$;