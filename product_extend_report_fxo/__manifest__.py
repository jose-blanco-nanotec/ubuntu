{
    'name': 'Producto reporte',
    'version': '11.0.1.0.0',
    'author': 'Flexxoone',
    'license': 'AGPL-3',
    'summary': 'Agregar campo a reporte',
    'category': 'Product',
    'depends': ['product', 'sale'],
    'description': """
                Agrega nuevos campos en reporte de ventas y punto de venta  (Textil)
                - Modelo, Estilo, Tela, Producto
                - Concatena en campo name
    """,

    'data': [
        'views/product_view.xml',
    ],
    'auto_install': False,
    'installable': True,
}
