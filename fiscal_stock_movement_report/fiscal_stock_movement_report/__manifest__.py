# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name':'Fiscal Stock Movement Report',
    'version' : '1.0',
    'category': 'Inventory',
    'author': "GYB IT SOLUTIONS",
    'website': 'http://www.gybitsolutions.com/',
    'license': 'AGPL-3',
    'summary': "Report has full details of stock movement",
    'description': """User can print stock movement report in PDF and Excel format.
                      In the report User can get full informations like Document type, Document number, Incoming_qty, Outgoing_qty, Stock price, Client name etc.   """,
    'depends': ['stock', 'account', 'web'],
    'data': [
        'views/menu_report.xml',
        'views/report_fiscal_stock_movement.xml',
        'wizard/fiscal_stock_movement_view.xml',
    ],
    'demo': [],
    'price': 10.0,
    'currency': 'EUR',
    'installable' : True,
    'auto_install' : False,
    'application' : True,
    'images': ['static/description/icon.png',],
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
