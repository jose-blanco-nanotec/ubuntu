# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import base64
import datetime
#from cStringIO import StringIO
from io import BytesIO     # for handling byte strings
from io import StringIO    # for handling unicode strings

import xlwt
from odoo import api, fields, models, _
from odoo.tools.misc import ustr
import json
import itertools
import operator
from odoo.exceptions import ValidationError

class fiscal_stock_movement(models.TransientModel):
    _name = 'fiscal.stock.movement'

    start_date = fields.Datetime(string='Start Date', required=True, default=datetime.datetime.now())
    end_date = fields.Datetime(string='End Date', required=True, default=datetime.datetime.now())
    product_id = fields.Many2one('product.product', string='Product', required=True, )
    location_id = fields.Many2one('stock.location', string='Location', required=True, )

    def print_data_pdf(self, data):
        data = {}
        data['form']= self.read(['start_date', 'end_date', 'product_id', 'location_id'])[0]
        data['form'].update({'user':self.create_uid.name})
        return self.env.ref('fiscal_stock_movement_report.menu_fiscal_stock_movement_report_id').report_action(self,
                                                                                                data=data,
                                                                                                config=False)
    @api.multi
    @api.constrains('start_date', 'end_date')
    def _check_date(self):
        if str(self.start_date) > str(self.end_date):
            raise ValidationError(_('Start date must be less than end date.'))
        if (str(self.start_date) > str(datetime.datetime.now())) and (str(self.end_date) > str(datetime.datetime.now())):
            raise ValidationError(_('Start date and End date not be greater than current date.'))

    @api.multi
    def print_data_excel(self):
        filename = 'Fiscal Stock Movement Report.xlsx'
        workbook = xlwt.Workbook()
        style = xlwt.XFStyle()
        style_center = xlwt.easyxf(
            'align:vertical center, horizontal center; font:bold on; pattern: pattern solid, fore_colour gray25;')
        style_product = xlwt.easyxf('font:bold on; pattern: pattern solid, fore_colour gray25; border: top thin; ')
        style_location = xlwt.easyxf('font:bold on;pattern: pattern solid, fore_color white;')
        style_general = xlwt.easyxf('font:bold on;align: horizontal right; pattern: pattern solid, fore_colour gray25;')
        style_border = xlwt.easyxf('border: top thin, bottom thin, right thin, left thin')
        style_company_name = xlwt.easyxf('font:height 300, bold on; pattern: pattern solid, fore_color white;')
        style_vat = xlwt.easyxf('font:height 250, bold on; pattern: pattern solid, fore_color white;')
        style_title = xlwt.easyxf(
            'font:height 300, bold on; align:horizontal center, vertical center; pattern: pattern solid, fore_color white; ')
        style_date = xlwt.easyxf(
            'font:height 250, bold on; align:horizontal center, vertical center; pattern: pattern solid, fore_color white;')
        style_datetime = xlwt.easyxf('pattern: pattern solid, fore_color white;')

        font = xlwt.Font()
        font.name = 'Times New Roman'
        font.bold = True
        font.height = 250
        style.font = font
        worksheet = workbook.add_sheet('Sheet 1')
        user_obj = self.env['res.users'].sudo().browse(self._uid)

        company_name = 'Company Name : ' + user_obj.company_id.name
        worksheet.write_merge(0, 0, 0, 10, ustr(company_name), style_company_name)
        worksheet.row(0).height = 256 * 2

        vat_data = user_obj.company_id.company_registry or ''
        vat = 'Vat Number : ' + vat_data
        worksheet.write_merge(1, 1, 0, 10, ustr(vat), style_vat)
        worksheet.row(1).height = 256 * 2

        partner = 'Nrc(Res.Partner) : ' + user_obj.name
        worksheet.write_merge(2, 2, 0, 10, ustr(partner), style_vat)
        worksheet.row(2).height = 256 * 2

        worksheet.write_merge(3, 3, 0, 10, '', style_vat)
        worksheet.write_merge(4, 4, 0, 10, '', style_vat)
        worksheet.write_merge(5, 5, 0, 10, '', style_vat)

        worksheet.write_merge(6, 6, 0, 10, 'Product Analytic Form (Stock moves)', style_title)
        worksheet.row(6).height = 256 * 3

        date_str = 'From ' + str(self.start_date) + ' To ' + str(self.end_date)
        worksheet.write_merge(7, 7, 0, 10, ustr(date_str), style_date)
        worksheet.row(7).height = 256 * 2

        worksheet.write_merge(8, 8, 0, 10, '', style_vat)
        worksheet.write_merge(9, 9, 0, 10, str(datetime.datetime.now()), style_datetime)

        worksheet.write(10, 0, '', style_center)
        worksheet.write(10, 1, '', style_center)
        worksheet.write(10, 2, 'Document', style_center)
        worksheet.write(10, 3, '', style_center)
        worksheet.write(10, 4, '', style_center)
        worksheet.write(10, 5, 'Quantity', style_center)
        worksheet.write(10, 6, '', style_center)
        worksheet.write(10, 7, 'Price', style_center)
        worksheet.write(10, 8, 'Account', style_center)
        worksheet.col(8).width = 256 * 20
        worksheet.write(10, 9, 'Client', style_center)
        worksheet.write(10, 10, 'BarCode', style_center)

        worksheet.write(11, 0, 'No.', style_center)
        worksheet.write(11, 1, 'Document Type', style_center)
        worksheet.col(1).width = 256 * 20
        worksheet.write(11, 2, 'Number', style_center)
        worksheet.col(2).width = 256 * 20
        worksheet.write(11, 3, 'Date', style_center)
        worksheet.write(11, 4, 'Incoming', style_center)
        worksheet.write(11, 5, 'Outgoing', style_center)
        worksheet.write(11, 6, 'Stock', style_center)
        worksheet.write(11, 7, '', style_center)
        worksheet.write(11, 8, '', style_center)
        worksheet.write(11, 9, '', style_center)
        worksheet.write(11, 10, '', style_center)

        product_name = 'Product: ' + self.product_id.name
        if self.product_id.default_code:
            product_name = product_name + ' - ' + self.product_id.default_code
        worksheet.write_merge(12, 12, 0, 10, ustr(product_name), style_product)

        incoming_qty_stock = 0
        outgoing_qty_stock = 0
        incoming_prev_stock_move_ids = self.env['stock.move'].search([('product_id', '=', self.product_id.id),
                                                                      ('date', '>=', self.start_date),
                                                                      ('date', '<=', self.end_date),
                                                                      ('location_dest_id', '=', self.location_id.id)])
        if incoming_prev_stock_move_ids:
            for in_pre_stock_move in incoming_prev_stock_move_ids:
                incoming_qty_stock += in_pre_stock_move.product_uom_qty

        outgoing_prev_stock_move_ids = self.env['stock.move'].search([('product_id', '=', self.product_id.id),
                                                                      ('date', '>=', self.start_date),
                                                                      ('date', '<=', self.end_date),
                                                                      ('location_id', '=', self.location_id.id)])
        if outgoing_prev_stock_move_ids:
            for out_pre_stock_move in outgoing_prev_stock_move_ids:
                outgoing_qty_stock += out_pre_stock_move.product_uom_qty

        initial_stock = incoming_qty_stock - outgoing_qty_stock
        balance_stock = initial_stock

        worksheet.write(13, 0, 1, style_border)
        worksheet.write(13, 1, 'Initial Stock', style_border)
        worksheet.write(13, 2, 0, style_border)
        worksheet.write(13, 3, str(self.start_date).split(' ')[0], style_border)
        worksheet.write(13, 4, 0, style_border)
        worksheet.write(13, 5, 0, style_border)
        worksheet.write(13, 6, initial_stock, style_border)
        worksheet.write(13, 7, self.product_id.lst_price, style_border)
        worksheet.write(13, 8, '', style_border)
        worksheet.write(13, 9, '', style_border)
        worksheet.write(13, 10, '', style_border)

        stock_move_ids = self.env['stock.move'].search([('product_id', '=', self.product_id.id),
                                                        ('date', '>=', self.start_date),
                                                        ('date', '<=', self.end_date),
                                                        ('location_id', '=', self.location_id.id)])
        stock_move_list = []
        for stock_move in stock_move_ids:
            stock_move_dict = {
                'date': stock_move.date,
                'available_qty': stock_move.remaining_qty,
                'price': stock_move.price_unit,
                'barcode': '',
                'product_categ_id': self.product_id.categ_id.name_get()[0][1]
            }
            if stock_move.picking_type_id:
                stock_move_dict.update({
                    'document_type': stock_move.picking_type_id.name,
                    'document_number': stock_move.picking_id.name,
                })
                if stock_move.purchase_line_id:
                    stock_move_dict.update({
                        'incoming_qty': stock_move.product_uom_qty,
                        'outgoing_qty': 0.0,
                        'partner_id': stock_move.purchase_line_id.order_id.partner_id.name,
                    })
                if stock_move.sale_line_id:
                    stock_move_dict.update({
                        'incoming_qty': 0.0,
                        'outgoing_qty': stock_move.product_uom_qty,
                        'partner_id': stock_move.sale_line_id.order_id.partner_id.name,
                    })
            stock_move_list.append(stock_move_dict)

        row = 14
        newlist = sorted(stock_move_list, key=lambda k: k['product_categ_id'])
        groups = itertools.groupby(newlist, key=operator.itemgetter('product_categ_id'))
        result = [{'product_categ_id': k, 'values': [x for x in v]} for k, v in groups]
        index = 1
        final_incoming_qty_total = 0
        final_outgoing_qty_total = 0
        for res in result:
            reslist = sorted(res['values'], key=lambda k: k['price'])
            resgroups = itertools.groupby(reslist, key=operator.itemgetter('price'))
            resresult = [{'price': k, 'values': [x for x in v]} for k, v in resgroups]

            for res_res in resresult:
                res_res.update({'product_categ_id': res['product_categ_id']})
            for final in resresult:
                if final['product_categ_id']:
                    location_account_price = 'Location: ' + self.location_id.name_get()[0][1] + ',       Category Name: ' + final[
                        'product_categ_id'] + ',       Price: ' + str(final['price'])
                    worksheet.write_merge(row, row, 0, 10, location_account_price, style_location)
                    row += 1
                    inner_incoming_qty_total = 0
                    inner_outgoing_qty_total = 0
                    for dict in final['values']:
                        index += 1
                        worksheet.write(row, 0, index, style_border)
                        worksheet.write(row, 1, dict['document_type'], style_border)
                        worksheet.write(row, 2, dict['document_number'], style_border)
                        worksheet.write(row, 3, str(dict['date']).split(' ')[0], style_border)
                        worksheet.write(row, 4, dict['incoming_qty'], style_border)
                        inner_incoming_qty_total += dict['incoming_qty']
                        worksheet.write(row, 5, dict['outgoing_qty'], style_border)
                        inner_outgoing_qty_total += dict['outgoing_qty']
                        balance_stock = balance_stock + dict['incoming_qty'] - dict['outgoing_qty']
                        worksheet.write(row, 6, balance_stock, style_border)
                        worksheet.write(row, 7, dict['price'], style_border)
                        worksheet.write(row, 8, dict['product_categ_id'], style_border)
                        worksheet.write(row, 9, dict['partner_id'], style_border)
                        worksheet.write(row, 10, dict['barcode'], style_border)
                        row += 1

                    worksheet.write(row, 0, '', style_location)
                    worksheet.write(row, 1, '', style_location)
                    worksheet.write(row, 2, '', style_location)
                    worksheet.write(row, 3, 'Total', style_location)
                    worksheet.write(row, 4, inner_incoming_qty_total, style_location)
                    worksheet.write(row, 5, inner_outgoing_qty_total, style_location)
                    worksheet.write(row, 6, '', style_location)
                    worksheet.write(row, 7, '', style_location)
                    worksheet.write(row, 8, '', style_location)
                    worksheet.write(row, 9, '', style_location)
                    worksheet.write(row, 10, '', style_location)

                    row += 1
                    final_incoming_qty_total += inner_incoming_qty_total
                    final_outgoing_qty_total += inner_outgoing_qty_total

        worksheet.write_merge(row, row, 0, 3, 'TOTAL GENERAL:', style_general)
        worksheet.write(row, 4, final_incoming_qty_total, style_center)
        worksheet.write(row, 5, final_outgoing_qty_total, style_center)
        worksheet.write_merge(row, row, 6, 10, '', style_center)

        fp = StringIO()
        workbook.save(fp)
        export_id = self.env['fiscal.stock.movement.export'].create(
            {'excel_file': base64.encodestring(fp.getvalue()), 'file_name': filename})
        fp.close()

        return {
            'view_mode': 'form',
            'res_id': export_id.id,
            'res_model': 'fiscal.stock.movement.export',
            'view_type': 'form',
            'type': 'ir.actions.act_window',
            'context': self._context,
            'target': 'new',
        }


class fiscal_stock_movement_export(models.TransientModel):
    _name = 'fiscal.stock.movement.export'

    excel_file = fields.Binary(string='Download')
    file_name = fields.Char(string='File name', size=64)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
