# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import time
import datetime
from odoo import api, models, _
from odoo.exceptions import UserError
import json
import itertools
import operator

class fiscal_stock_movement_report(models.AbstractModel):
    _name = 'report.fiscal_stock_movement_report.report_fiscal_stock'
    
    def get_stock(self, product_id, start_date, end_date, location_id):        
        incoming_qty_stock = 0
        outgoing_qty_stock = 0

        incoming_prev_stock_move_ids = self.env['stock.move'].search([('product_id', '=', product_id),
                                                                      ('date', '>=', start_date),
                                                                      ('date', '<=', end_date),
                                                                      ('location_dest_id', '=', location_id)])
        if incoming_prev_stock_move_ids:
            for in_pre_stock_move in incoming_prev_stock_move_ids:
                incoming_qty_stock += in_pre_stock_move.product_uom_qty

        outgoing_prev_stock_move_ids = self.env['stock.move'].search([('product_id', '=', product_id),
                                                                      ('date', '>=', start_date),
                                                                      ('date', '<=', end_date),
                                                                      ('location_id', '=', location_id)])
        if outgoing_prev_stock_move_ids:
            for out_pre_stock_move in outgoing_prev_stock_move_ids:
                outgoing_qty_stock += out_pre_stock_move.product_uom_qty

        initial_stock = incoming_qty_stock - outgoing_qty_stock
        return initial_stock

    def location_account_price(self, vals, location_id):
        location = self.env['stock.location'].browse(location_id[0]).name_get()[0][1]
        result = []
        if vals['product_categ_id']:
            location_account_price = 'Location: ' + location + ', Category Name: ' + vals[
                'product_categ_id'] + ', Price: ' + str(vals['price'])
        result.append(location)
        result.append(vals['product_categ_id'])
        result.append(str(vals['price']))
        return result

    def get_stock_data(self, product_id, start_date, end_date, location_id):
        stock_move_ids = self.env['stock.move'].search([('product_id', '=', product_id),
                                                        ('date', '>=', start_date),
                                                        ('date', '<=', end_date),
                                                        ('location_id', '=', location_id)])
        product_obj = self.env['product.product'].browse(product_id)
        stock_move_list = []
        resresult = []
        for stock_move in stock_move_ids:
            stock_move_dict = {
                'date': stock_move.date,
                'available_qty': stock_move.remaining_qty,
                'price': stock_move.price_unit,
                'barcode': '',
                'product_categ_id': product_obj.categ_id.name_get()[0][1]
            }
            if stock_move.picking_type_id:
                stock_move_dict.update({
                    'document_type': stock_move.picking_type_id.name,
                    'document_number': stock_move.picking_id.name,
                })
                if stock_move.purchase_line_id:
                    stock_move_dict.update({
                        'incoming_qty': stock_move.product_uom_qty,
                        'outgoing_qty': 0.0,
                        'partner_id': stock_move.purchase_line_id.order_id.partner_id.name,
                    })
                if stock_move.sale_line_id:
                    stock_move_dict.update({
                        'incoming_qty': 0.0,
                        'outgoing_qty': stock_move.product_uom_qty,
                        'partner_id': stock_move.sale_line_id.order_id.partner_id.name,
                    })
            stock_move_list.append(stock_move_dict)
            newlist = sorted(stock_move_list, key=lambda k: k['product_categ_id'])
            groups = itertools.groupby(newlist, key=operator.itemgetter('product_categ_id'))
            result = [{'product_categ_id': k, 'values': [x for x in v]} for k, v in groups]
            for res in result:
                reslist = sorted(res['values'], key=lambda k: k['price'])
                resgroups = itertools.groupby(reslist, key=operator.itemgetter('price'))
                resresult = [{'price': k, 'values': [x for x in v]} for k, v in resgroups]
                for res_res in resresult:
                    res_res.update({'product_categ_id': res['product_categ_id']})
        return resresult

    @api.model
    def get_report_values(self, docids, data=None):
        if not data.get('form') or not self.env.context.get('active_model') or not self.env.context.get('active_id'):
            raise UserError(_("Form content is missing, this report cannot be printed."))

        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))

        return {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'data': data['form'],
            'docs': docs,
            'time': time,
            'get_stock': self.get_stock,
            'location_account_price': self.location_account_price,
            'get_stock_data': self.get_stock_data,
        }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
