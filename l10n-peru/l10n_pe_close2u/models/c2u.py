# -*- coding: utf-8 -*-
import json
from collections import OrderedDict
import requests
import logging
import re

_logger = logging.getLogger(__name__)

class Close2U(object):
    
    def __init__(self):
        self.value=OrderedDict()
        self.value['close2u']={}
        self.value['close2u']['tipoIntegracion']="ERP"
        self.value['close2u']['tipoRegistro']="PRECIOS_CON_IGV"    
    
    def getIinformacionAdicional(self, invoice):
        vals=OrderedDict()
        vals['condicionPago']= ""  #invoice.payment_term_id and invoice.payment_term_id.name or ""
        vals['fechaEmision']=invoice.date_invoice
        if invoice.date_due:
            vals['fechaVencimiento']=invoice.date_due
        vals['formaPago']= invoice.payment_term_id and invoice.payment_term_id.c2y_payment_method or ""
        if invoice.c2u_invoice_code in ['NOTADEBITO', 'NOTACREDITO']:
            vals['glosa']= invoice.name or invoice.invoice_line_ids[0].name or ""
        else:
            vals['glosa']= ""
        vals['moneda']=invoice.currency_id.name
        vals['numero']=int(invoice.number.split("-")[1])
        vals['ordencompra']= invoice.origin or ""
        vals['serie']=invoice.number.split("-")[0]
        self.value['datosDocumento']=vals
    
    def getOriginInvoice(self, invoice):
        parent= invoice.refund_invoice_id or invoice.debit_invoice_id or None
        if parent:
            vals=OrderedDict()
            vals['fechaEmision']= parent.date_invoice
            vals['numero']=int(parent.internal_number.split("-")[1])
            vals['serie']=parent.internal_number.split("-")[0]
            vals['tipoDocumento']= parent.c2u_invoice_code
            self.value['comprobanteAjustado']=vals
            self.value['motivo']=invoice.c2u_debit_note_code or invoice.c2u_credit_note_code or ""
    
    def getDomicilioFiscal(self, partner_id):
        vals=OrderedDict()
        vals["departamento"]= partner_id.state_id.name or ""
        vals["direccion"]= partner_id.street or ""
        vals["distrito"]= partner_id.district_id.name or ""
        vals["pais"]= "PERU"
        vals["provincia"]= partner_id.province_id.name or ""
        vals["ubigeo"]= partner_id.district_id and partner_id.district_id.code[2:] or ""
        vals["urbanizacion"]= partner_id.street2 or ""
        return vals
    
    def getPartner(self, partner_id):
        vals=OrderedDict()
        vals["correo"]= partner_id.email or ""
        vals["correoCopia"]= ""
        vals["domicilioFiscal"]= self.getDomicilioFiscal(partner_id)
        vals["nombreComercial"]= partner_id.commercial_name and partner_id.commercial_name!="-" and partner_id.commercial_name or ""
        vals["nombreLegal"]= partner_id.legal_name and partner_id.legal_name!="-" and partner_id.legal_name or partner_id.name or ""
        vals["numeroDocumentoIdentidad"]= partner_id.doc_number or ""
        vals["tipoDocumentoIdentidad"]= partner_id.doc_type and partner_id.get_c2u_doc_type() or ""
        return vals
    
    def getInformacionAdicional(self, invoice):
        vals=OrderedDict()
        vals["coVendedor"]= ""
        vals["vendedor"]= invoice.user_id.name or ""
        self.value['informacionAdicional']=vals
    
    def getPercepcion(self, invoice):
        vals= {"porcentaje": ""}
        self.value['percepcion']=vals
        
    def getDetalleDocumento(self, invoice):
        lines=[]
        for line in invoice.invoice_line_ids:
            val=OrderedDict()
            val["cantidad"]=line.quantity
            val["codigoProducto"]=line.product_id.default_code or '-'
            val["descripcion"]=line.name
            if  line.discount<100.00 and line.discount>0:
                val["descuento"]= {"monto": line.amount_discount}
            if False:
                val["isc"]= {
                    "moneda": "requerido",
                    "monto": "requerido",
                    "tipoSistemaCalculo": "requerido"
                  }
            if line.invoice_line_tax_ids:
                val["precioVentaUnitarioItem"]= line.get_price_unit()['total_included']
            else:
                val["valorReferencialUnitarioItem"]= line.product_id and line.product_id.standard_price or  line.get_price_unit()['total_excluded']
            if  line.discount==100.00:
                val["valorVentaUnitarioItem"]=  line.get_price_unit()['total_excluded']
            val["tipoAfectacion"]= line.c2u_affectation_code
            val["unidadMedida"]= line.uom_id and line.uom_id.c2u_code or "UNIDAD_BIENES"
            lines.append(val)
        self.value['detalleDocumento']=lines
    
    def getReferencias(self, invoice):
        val={
              "documentoReferenciaList": [
                {
                  "fechaEmision": "requerido",
                  "numero": "requerido",
                  "serie": "requerido",
                  "tipoDocumento": "requerido"
                }
              ]
            }
    def getInformacionAdicional(self, invoice):
        val = {
            "tipoOperacion": invoice.c2u_operation_type
        }
        self.value['informacionAdicional']=val

    def getSector(self, invoice):
        val={
            "tipoCargo": "",
            "tipoTotalDescuentos": ""
          }
        self.value['sector']=val
    
    def getAnticipos(self, invoice):
        val =[
            {
              "documento": {
                "numero": "requerido",
                "serie": "requerido",
                "tipoDocumento": "requerido"
              },
              "totalIgv": "requerido",
              "totalVentaExonerada": "requerido",
              "totalVentaGravada": "requerido",
              "totalVentaInafecta": "requerido"
            }
          ]

    def getInvoiceGuide(self, invoice):
        vals=OrderedDict()
        if invoice.c2u_is_eguide:
            for stock in invoice.pe_stock_ids:
                vals['licenciaConducir']=None
                vals['modalidadTransporte']=stock.c2u_transport_mode
                vals['puntoLlegada']=self.getDomicilioFiscal(stock.partner_id)
                vals['puntoPartida']=self.getDomicilioFiscal(stock.picking_type_id.warehouse_id.partner_id)
                vals['totalPesoBruto']={
                                          "codigo": "KG",
                                          "moneda": None,
                                          "monto": stock.c2u_unit_quantity
                                        }
                if stock.c2u_transport_mode=="01":
                    vals['transportista']= {
                                              "nombreLegal": stock.c2u_carrier_id.name,
                                              "numeroDocumentoIdentidad": stock.c2u_carrier_id.doc_number,
                                              "tipoDocumentoIdentidad": stock.c2u_carrier_id.get_c2u_doc_type()
                                            }
                else:
                    for line in stock.c2u_fleet_ids:
                        vals['transportista']= {
                                                  "nombreLegal": line.driver_id.name,
                                                  "numeroDocumentoIdentidad": line.driver_id.doc_number,
                                                  "tipoDocumentoIdentidad": line.driver_id.get_c2u_doc_type()
                                                }
                        vals['vehiculo']= {
                                      "certificadoVehicular": "1",
                                      "marca": line.fleet_id.model_id and line.fleet_id.model_id.name_get() and line.fleet_id.model_id.name_get()[0][1] or None,
                                      "placa": line.name
                                    }
                        break
                break
            self.value['facturaGuia'] =  vals
    
    def getInvoice2U(self, invoice):
        self.getIinformacionAdicional(invoice)
        self.getOriginInvoice(invoice)
        self.getInvoiceGuide(invoice)
        self.value['emisor']=self.getPartner(invoice.company_id.partner_id)
        self.value['receptor']= self.getPartner(invoice.partner_id)
        self.getInformacionAdicional(invoice)
        #self.value["otrosCargos"]  = ""
        #self.getPercepcion(invoice)
        #self.value["descuentoGlobal"] = ""
        self.getDetalleDocumento(invoice)
        #self.getReferencias(invoice)
        self.getSector(invoice)
        #self.getAnticipos(invoice)
        return self.value

    def getLowComunication(self, batch):
        self.value['close2u']=None
        self.value['datosDocumento']=None
        self.value['descuentoGlobal']=None
        self.value['detalleDocumento']=[]
        self.value['emisor']=self.getPartner(batch.company_id.partner_id)
        self.value['informacionAdicional']=None
        self.value['receptor']=None
        self.value['referencias']=None
        lows=[]
        for invoice in batch.voided_ids:
            val={'id':None}
            val['motivo']= invoice.name or "Cancelado"
            val['numero']=int(invoice.internal_number.split("-")[1])
            val['serie']=invoice.internal_number.split("-")[0]
            val['tipoComprobante']=invoice.get_sunat_doc_type()
            lows.append(val)
        self.value['resumenBajaItemList']=lows
        return self.value


class SendClose2U(object):
    
    def __init__(self, url):
        self._url = "%sapiemisor/" % url
        self._method= None
        self._type= None

    def search(self, batch):
        val=OrderedDict()
        val["emisor"]=batch.company_id.partner_id.doc_number
        val["numero"]=batch.number
        val["serie"]=batch.series
        # doc_type=None
        #         if batch.type=="sync":
        #             type=batch.invoice_ids[0].journal_id.c2u_invoice_code
        #             if type=="NOTACREDITO":
        #                 doc_type="NOTA_CREDITO"
        #             elif type=="NOTADEBITO":
        #                 doc_type="NOTA_DEBITO"
        #             elif type=="FACTURA":
        #                 doc_type="FACTURA"
        #             else:
        #                 doc_type="BOLETA"
        #         elif batch.type=="ra":
        #             doc_type="RESUMEN_BAJAS"
        #         else:
        #             doc_type="RESUMEN_DIARIO"
        val["tipoComprobante"]=batch.get_sunat_doc_type()
        return json.dumps(val)
    
    def _getApiMethod(self):
        if self._type=="FACTURA":
            self._method="invoice2u/integracion/factura"
        elif self._type=="BOLETA":
            self._method="invoice2u/integracion/boleta"
        elif self._type=="NOTACREDITO":
            self._method="invoice2u/integracion/nota-credito"
        elif self._type=="NOTADEBITO":
            self._method="invoice2u/integracion/nota-debito"
        elif self._type=="rc":
            self._method="invoice2u/integracion/resumen-diario"
        elif self._type=="ra":
            self._method="invoice2u/integracion/resumen-baja"
        elif self._type=="search":
            self._method="invoice2u/integracion/buscar"
        elif self._type=="pdf":
            self._method="invoice2u/integracion/consultarPdf"
    
    def send(self, type, data, headers):
        #headers['content-type'] = 'application/json'
        self._type=type
        self._getApiMethod()
        _logger.info(data)
        if self._type!="search":
            response = requests.put('%s%s' %(self._url.encode('utf-8'), self._method.encode('utf-8')), 
                                     data=data.encode('utf-8'), headers=headers)
        else:
            response = requests.post('%s%s' %(self._url.encode('utf-8'), self._method.encode('utf-8')), 
                                     data=data.encode('utf-8'), headers=headers)
        _logger.info(response.text)
        if response.status_code!=200:
            return False, response
        return True, response

def getDocument(self):
    if self.type=="sync":
        res = Close2U().getInvoice2U(self.invoice_ids[0])
    else:
        res = Close2U().getLowComunication(self)
    return json.dumps(res)

def sendDocument(self):
    url = self.company_id.c2u_cpe_server_id.url
    headers= self.prepare_c2u_auth()
    if self.type=="sync":
        type= self.invoice_ids[0].c2u_invoice_code
    else:
        type= self.type
    data = self.send_document.encode('utf-8')
    return SendClose2U(url).send(type, data, headers)

def searchDocument(self):
    url = self.company_id.c2u_cpe_server_id.url
    headers= self.prepare_c2u_auth()
    send=SendClose2U(url)
    data=send.search(self)
    return send.send("search", data, headers)

def getPdfDocument(self):
    url = self.company_id.c2u_cpe_server_id.url
    headers= self.prepare_c2u_auth()
    send=SendClose2U(url)
    data=send.search(self)
    return send.send("pdf", data, headers)

    