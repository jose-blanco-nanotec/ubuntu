# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class ProductUoM(models.Model):
    _inherit = "product.uom"

    c2u_code = fields.Selection(selection="_get_c2u_code", string="Close2u Unit Code")

    @api.model
    def _get_c2u_code(self):
        return self.env['pe.datas'].get_selection("PE.CATALOG.UM")
