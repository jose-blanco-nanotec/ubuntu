# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
import odoo.addons.decimal_precision as dp
from base64 import encodestring
from odoo.exceptions import UserError
import re

class Picking(models.Model):
    _inherit = "stock.picking"
    
    @api.model
    def _get_c2u_transport_mode(self):
        return self.env['pe.datas'].get_selection("PE.C2U.CATALOG18")
    
    c2u_is_eguide = fields.Boolean("Is EGuide", copy=False)
    c2u_eguide_generated = fields.Boolean("Is Generated", copy=False)
    
    c2u_gross_weight = fields.Float("Gross Weigh", digits=dp.get_precision('Product Unit of Measure'), copy=False)
    c2u_unit_quantity = fields.Integer("Unit Quantity", copy=False)
    c2u_transport_mode = fields.Selection(selection="_get_c2u_transport_mode", string="Transport Mode", copy=False)
    c2u_carrier_id = fields.Many2one(comodel_name="res.partner", string="Carrier", copy=False)
    c2u_fleet_ids = fields.One2many(comodel_name="c2u.stock.fleet", inverse_name="picking_id", string="Fleet Private", copy=False)
    
    @api.multi
    def do_new_transfer(self):
        res=super(Picking, self).do_new_transfer()
        if not self.c2u_gross_weight:
            self.c2u_gross_weight=sum([line.product_id.weight for line in self.pack_operation_ids])
        if not self.c2u_unit_quantity:
            self.c2u_unit_quantity= sum([line.qty_done or line.product_qty for line in self.pack_operation_ids])
        return res

    @api.one
    def validate_eguide(self):
        if not self.partner_id:
            raise UserError(_("Customer is required"))
        if self.partner_id.id == self.company_id.partner_id.id:
            raise UserError("Destinatario no debe ser igual al remitente")
        if not self.partner_id.parent_id.doc_type and not self.partner_id.doc_type:
            raise UserError(_("Customer type document is required"))
        if not self.partner_id.parent_id.doc_number and not self.partner_id.doc_number:
            raise UserError(_("Customer number document is required"))
        if not self.partner_id.street:
            raise UserError(_("Customer street is required for %s") %(self.partner_id.name or ""))
        if not self.partner_id.district_id:
            raise UserError(_("Customer district is required for %s")  %(self.partner_id.name or ""))
        
        if not self.c2u_carrier_id.doc_type and self.c2u_transport_mode=="01":
            raise UserError(_("Carrier type document is required for %s") %(self.c2u_carrier_id.name or ""))
        if not self.c2u_carrier_id.doc_number and self.c2u_transport_mode=="01":
            raise UserError(_("Carrier number document is required for %s") %(self.c2u_carrier_id.name or ""))
        if not self.picking_type_id.warehouse_id.partner_id or not self.picking_type_id.warehouse_id.partner_id.street:
            raise UserError(_("It is necessary to enter the warehouse address for %s") %(self.picking_type_id.warehouse_id.partner_id.name or ""))
        if self.picking_type_id.warehouse_id.partner_id and not self.picking_type_id.warehouse_id.partner_id.district_id:
            raise UserError(_("It is necessary to enter the warehouse district for %s") %(self.picking_type_id.warehouse_id.partner_id.name or ""))
        if self.c2u_transport_mode=="02" and len(self.c2u_fleet_ids)>0:
            for line in self.c2u_fleet_ids:
                if not line.driver_id.doc_type:
                    raise UserError(_("Carrier type document is required for %s") %(line.driver_id.name or ""))
                if not line.driver_id.doc_number:
                    raise UserError(_("Carrier number document is required for %s") %(line.driver_id.name or ""))
        elif self.c2u_transport_mode=="02" and len(self.c2u_fleet_ids)==0:
            raise UserError(_("It is necessary to add a vehicle and driver"))

class PeStockFleet(models.Model):
    _name = "c2u.stock.fleet"
    
    name = fields.Char("License Plate", required=True)
    fleet_id = fields.Many2one(comodel_name="fleet.vehicle", string="Vehicle")
    picking_id = fields.Many2one(comodel_name="stock.picking", string="Picking")
    driver_id = fields.Many2one(comodel_name="res.partner", string="Driver", required=True)
    
    @api.onchange("fleet_id")
    def onchange_fleet_id(self):
        if self.fleet_id:
            self.name = self.fleet_id.license_plate
            self.driver_id = self.fleet_id.driver_id.id
    