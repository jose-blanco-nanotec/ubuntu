# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, Warning
from .c2u import getDocument, sendDocument, searchDocument, getPdfDocument
from collections import OrderedDict
from base64 import b64decode, b64encode

class pe_close2u(models.Model):
    _name = 'pe.close2u'

    name = fields.Char("Name", readonly=True, default="/")
    state = fields.Selection([
            ('draft','Draft'),
            ('generate', 'Generated'),
            ('send', 'Send'),
            ('verify', 'Waiting'),
            ('done', 'Done'),
            ('cancel', 'Cancelled'),
        ], string='Status', index=True, readonly=True, default='draft',
        track_visibility='onchange', copy=False)
    type = fields.Selection([
        ('sync', 'Synchronous'),
        ('rc', 'Daily Summary'),
        ('ra', 'Low communication'),
        ], string="Type", default='sync')
    date = fields.Date("Date", default=fields.Date.context_today)
    company_id = fields.Many2one('res.company', string='Company', change_default=True,
            required=True, readonly=True, states={'draft': [('readonly', False)]},
            default=lambda self: self.env['res.company']._company_default_get('pe.close2u'))
    send_document = fields.Text("Send Document", readonly=True, states={'draft': [('readonly', False)]})
    response = fields.Char("Response", readonly=True)
    response_code = fields.Char("Response Code", readonly=True)
    note = fields.Text("Note", readonly=True)
    invoice_ids = fields.One2many("account.invoice", 'c2u_id', string="Invoices", readonly=True)
    date_end = fields.Datetime("End Date", states={'draft': [('readonly', False)]})
    send_date = fields.Datetime("Send Date", states={'draft': [('readonly', False)]})
    voided_ids = fields.One2many("account.invoice", "c2u_voided_id", string="Voided Invoices")
    summary_ids = fields.One2many("account.invoice", "c2u_summary_id", string="Summary Invoices")
    
    barcode_datas = fields.Binary("Barcode", readonly=True)
    barcode_fname = fields.Char("Barcode Name",  readonly=True)
    signature = fields.Char("Signature", readonly=True)
    hash = fields.Char("Hash", readonly=True)
    identifier = fields.Integer("Identifier", readonly=True)
    number = fields.Char("Number", readonly=True)
    series = fields.Char("series", readonly=True)
    doc_type = fields.Char("Document type", readonly=True)
    
    sunat_state = fields.Selection("_get_sunat_state", "Sunat State", readonly=True)
    voucher_state = fields.Selection("_get_voucher_state", "Voucher State", readonly=True)
    send_state = fields.Selection("_get_send_state", "Send State", readonly=True)
    
    invoice_datas = fields.Binary("Invoice", readonly=True)
    invoice_fname = fields.Char("Invoice Name",  readonly=True)
    
    _order = 'name, date'

    @api.multi
    def unlink(self):
        for batch in self:
            if batch.name!="/" and batch.state !="draft":
                raise Warning(_('You can only delete sent documents.'))
        return super(pe_close2u, self).unlink()
    
    @api.model
    def _get_sunat_state(self):
        return self.env['pe.datas'].get_selection("PE.I2U.ESTADO")
    
    @api.model
    def _get_voucher_state(self):
        return self.env['pe.datas'].get_selection("PE.SUNAT.ESTADO")
    
    @api.model
    def _get_send_state(self):
        return self.env['pe.datas'].get_selection("PE.CATALOG.ENTREGA")
    
    @api.one
    def action_draft(self):
        self.state='draft'
        
    @api.one
    def action_generate(self):
        if not self.send_document: 
            self.send_document=getDocument(self)
        self.state='generate'
        
    @api.one
    def action_send(self):
        if self.type=="sync" and self.name=="/":
            self.name= self.invoice_ids[0].number
        status, response = sendDocument(self)
        if not status:
            res = response.json()
            raise UserError("\n".join(res))
        res = response.json()
        #self.barcode_datas = res.get('codigobBarras', False)
        #if self.barcode_datas: 
        #    self.barcode_fname = "%s-%s-barcode.jpg" %(res.get('serie', ""),res.get('numero', "")) 
        self.signature = res.get('firma', False)
        self.hash = res.get('hash',False)
        if res.get('identificador',False):
            self.identifier = res.get('identificador')
        self.number = res.get('numero',False)
        self.series = res.get('serie',False)
        self.doc_type = res.get('tipoDocumento',False)
        self.state="send"
        try:
            self.check_document_status()
            self.action_report_pdf()
        except Exception:
            pass
    
    @api.multi
    def check_document_status(self):
        self.ensure_one()
        status, response = searchDocument(self)
        res = response.json()
        if not status:
            raise UserError("\n".join(res))
        if self.state=='send':
            self.state='verify'
        for result in res.get('result', []):
            doc_number= result.get("empresaId", False)
            series= result.get("serie", False)
            numero= str(result.get("numero", "")) or False
            
            batch = self.search([('company_id.partner_id.doc_number', '=', doc_number), ('number', '=', numero),
                                 ('series', '=', series)], limit=1)
            if batch:
                batch.sunat_state= result.get('estadoSunat')
                batch.voucher_state = result.get('estadoComprobante')
                batch.send_state= result.get('estadoEntrega')
                if batch.sunat_state=="1" and batch.voucher_state=="1" and batch.send_state=="1":
                    batch.state='done'
                else:
                    batch.state='verify'
    
    @api.one
    def get_pdf_close2u(self):
        if not self.invoice_datas:
            status, response = getPdfDocument(self)
            if not status:
                res = response.json()
                raise UserError("\n".join(res))
            self.invoice_datas = response.text 
            self.invoice_fname = "%s.pdf" %(self.name)
    
    @api.one
    def action_report_pdf(self):
        self.get_pdf_close2u()
    
    @api.one
    def action_verify(self):
        self.check_document_status()
        self.get_pdf_close2u()
    
    @api.one
    def action_done(self):
        self.check_document_status()
        self.get_pdf_close2u()
    
    @api.one
    def action_cancel(self):
        self.state='cancel'
    
    @api.model
    def create_from_invoice(self, invoice_id):
        vals={}
        vals['invoice_ids']= [(4, invoice_id.id)]
        vals['type'] = 'sync'
        vals['company_id']= invoice_id.company_id.id
        res=self.create(vals)
        return res
    
    @api.model
    def get_cpe_async(self, type, invoice_id):
        res=None
        company_id= invoice_id.company_id.id
        date_invoice= invoice_id.date_invoice
        cpe_id=self.search([('state', '=', 'draft'), ('type', '=', type), ('date', '=', date_invoice),
                            ('name', '=', "/"), ('company_id', '=', company_id)], limit=1, order="date DESC")
        if cpe_id:
            res=cpe_id
        else:
            vals={}
            vals['type'] = type
            vals['date'] = date_invoice
            vals['company_id']= company_id
            res=self.create(vals)
        return res
    
    @api.multi
    def get_sunat_doc_type(self):
        self.ensure_one()
        res = self.env['pe.datas'].search([('name', '=', self.invoice_ids[0].c2u_invoice_code), ('table_code', '=', 'PE.TIPO.DOCUMENTO.SUNAT')], limit=1)
        return res.code or ""
    
    @api.multi
    def prepare_c2u_auth(self):
        self.ensure_one()
        res=OrderedDict()
        res['j_ruc']= self.company_id.partner_id.doc_number
        res['j_user']= self.company_id.c2u_cpe_server_id.user
        res['j_email']= self.company_id.c2u_cpe_server_id.email
        res['j_password']= self.company_id.c2u_cpe_server_id.password
        res['content-type'] = 'application/json'
        return res
    
    @api.multi
    def check_close2u_status(self):
        batch_ids = self.search([('state', 'in', ['send', 'verify'])]) 
        for batch_id in batch_ids:
            try:
                batch_id.action_done()
            except Exception:
                pass
    
    @api.multi
    def send_close2u_status(self):
        batch_ids = self.search([('state', 'in', ['draft', 'generate']), ('type','=','ra')]) 
        for batch_id in batch_ids:
            batch_id.action_generate()
            batch_id.action_send()
            
            