# -*- encoding: utf-8 -*-
from odoo import api, fields, models, _
import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError
from base64 import encodestring
import re
from datetime import datetime, date, timedelta

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    
    c2u_id = fields.Many2one("pe.close2u", "Close2u Invoice", readonly=True, 
                             states={'draft': [('readonly', False)]}, copy=False)
    is_c2u = fields.Boolean("Is CPE", related="journal_id.is_c2u")
    c2u_invoice_code = fields.Selection(selection="_get_c2u_invoice_code", string="Invoice Type Code", 
                                       related="journal_id.c2u_invoice_code", readonly=True)
    c2u_voided_id= fields.Many2one("pe.close2u", "Voided Document", copy=False)
    c2u_summary_id = fields.Many2one("pe.close2u", "Summary Document", copy=False)
    
    c2u_debit_note_code = fields.Selection(selection="_get_pe_debit_note_type", string="Dedit Note Code", 
        states={'draft': [('readonly', False)]}, readonly=True)
    c2u_credit_note_code = fields.Selection(selection="_get_pe_credit_note_type", string="Credit Note Code",
        states={'draft': [('readonly', False)]}, readonly=True)
    c2u_operation_type = fields.Selection(selection="_get_c2u_operation_type", string="Operation Type", 
        states={'draft': [('readonly', False)]}, readonly=True, default="VENTA_INTERNA")
    c2u_invoice_datas = fields.Binary("Invoice", related="c2u_id.invoice_datas")
    c2u_invoice_fname = fields.Char("Invoice Name",  related="c2u_id.invoice_fname")

    #factura guia
    c2u_is_eguide = fields.Boolean("Is EGuide", copy=False, states={'draft': [('readonly', False)]}, 
                                   readonly=True)
    
    
    @api.model
    def _get_c2u_operation_type(self):
        return self.env['pe.datas'].get_selection("PE.TIPO.OPERACION")
    
    @api.model
    def _get_c2u_invoice_code(self):
        return self.env['pe.datas'].get_selection("PE.TIPO.DOCUMENTO")

    @api.model
    def _get_pe_credit_note_type(self):
        return self.env['pe.datas'].get_selection("PE.NOTA.CREDITO")
    
    @api.model
    def _get_pe_debit_note_type(self):
        return self.env['pe.datas'].get_selection("PE.NOTA.DEBITO")

    @api.one
    def validate_sunat_invoice(self):
        if not re.match(r'^(B|F){1}[A-Z0-9]{3}\-\d+$', self.number):
            raise UserError("El numero de la factura ingresada no cumple con el estandar.\n"\
                            "Verificar la secuencia del Diario por jemplo F001- o BN01-. \n"\
                            "Para cambiar ir a Configuracion/Contabilidad/Diarios/Secuencia del asiento")
        if self.c2u_invoice_code in ['BOLETA'] or self.debit_invoice_id.c2u_invoice_code in ['BOLETA'] or self.refund_invoice_id.c2u_invoice_code in ['BOLETA']:
            doc_type = self.partner_id.doc_type or '-'
            doc_number = self.partner_id.doc_number or '-'
            if doc_type == '6':
                raise UserError("El dato ingresado no cumple con el estandar \nTipo: %s \nNumero de documento: %s\n"\
                                "Deberia emitir una Factura. Cambiar en Factura/Otra Informacion/Diario"%(doc_type, doc_number))
            amount = self.company_id.sunat_amount or 0
            if self.amount_total >= amount and (doc_type=='-' or doc_number=='-'):
                raise UserError("El dato ingresado no cumple con el estandar \nTipo: %s \nNumero de documento: %s\nSon obligatorios el Tipo de Doc. y Numero"%(doc_type, doc_number))
        if self.c2u_invoice_code in ['FACTURA'] or self.debit_invoice_id.c2u_invoice_code in ['FACTURA'] or self.refund_invoice_id.c2u_invoice_code in ['FACTURA']:
            doc_type = self.partner_id.doc_type or '-'
            doc_number = self.partner_id.doc_number or '-'
            if doc_type != '6':
                raise UserError(" El numero de documento de identidad del receptor debe ser RUC \nTipo: %s \nNumero de documento: %s"%(doc_type, doc_number))
        if not self.partner_id.email and self.c2u_invoice_code in ['FACTURA']:
            raise UserError("El correo del receptor no debe ser vacio.")
        date_invoice = datetime.strptime(self.date_invoice,'%Y-%m-%d').date()
        today = fields.Date.context_today(self, datetime.now())
        days = datetime.strptime(today,'%Y-%m-%d').date() - date_invoice
        if days.days>6 or days.days<0:
            raise UserError("La fecha de emision no puede ser menor a 6 dias de hoy ni mayor a la fecha de hoy." )
        
    @api.multi
    def invoice_validate(self):
        res= super(AccountInvoice,self).invoice_validate()
        for invoice_id in self:
            if invoice_id.is_c2u and invoice_id.journal_id.c2u_invoice_code in ['FACTURA', 'BOLETA', 
                                                                               'NOTACREDITO', 'NOTADEBITO']:
                if invoice_id.c2u_is_eguide:
                    for stock in invoice_id.pe_stock_ids:
                        stock.validate_eguide()
                invoice_id.validate_sunat_invoice()
                if not invoice_id.c2u_id:
                    c2u_id = self.env['pe.close2u'].create_from_invoice(invoice_id)
                    invoice_id.c2u_id = c2u_id.id
                else:
                    c2u_id=invoice_id.c2u_id
                c2u_id.action_generate()
                c2u_id.action_send()
                
                #if cpe_id.error_code and (int(cpe_id.error_code)>=100 and cpe_id.error_code<=1999):
                #    raise UserError(_("The invoice was canceled due to:\n Error Code %s \n%s") % (cpe_id.error_code, cpe_id.note))
                #if cpe_id.error_code and (int(cpe_id.error_code)>=2000 and cpe_id.error_code<=3999):
                #    invoice_id.action_invoice_cancel()
                #if (invoice_id.journal_id.c2u_invoice_code in ["NOTACREDITO", "NOTADEBITO"] and invoice_id.origin_doc_code=="03") or invoice_id.journal_id.c2u_invoice_code == "BOLETA":
                #    c2u_summary_id=self.env['pe.close2u'].get_cpe_async("rc", invoice_id)
                #    invoice_id.c2u_summary_id=c2u_summary_id.id
        return res
    
    @api.model
    def _get_pe_error_code(self):
        return self.env['pe.datas'].get_selection("PE.CPE.ERROR")
    
    @api.multi
    def action_cancel(self):
        res = super(AccountInvoice, self).action_cancel()
        if res:
            for invoice_id in self:
                if invoice_id.c2u_id and invoice_id.c2u_id.state not in ["draft", "generate", "cancel"]:
                    date_invoice = datetime.strptime(invoice_id.date_invoice,'%Y-%m-%d').date()
                    today = fields.Date.context_today(self, datetime.now())
                    days = datetime.strptime(today,'%Y-%m-%d').date() - date_invoice
                    if days.days>3:
                        raise UserError("No puede cancelar este documento, solo se puede hacer antes de las 72 horas "\
                                        "contadas a partir del día siguiente de la fecha consignada en el CDR (constancia de recepción)."\
                                        "\nPara cancelar este Documento emita una Nota de Credito" )
                    if invoice_id.c2u_invoice_code in ['FACTURA', 'BOLETA']:
                        voided_id = self.env['pe.close2u'].get_cpe_async('ra', invoice_id)
                        invoice_id.c2u_voided_id = voided_id.id
    
    @api.multi
    def action_invoice_draft(self):
        res = super(AccountInvoice, self).action_invoice_draft()
        if self.filtered(lambda inv: inv.c2u_id and inv.c2u_id.state in ['send', 'verify', 'done']):
            raise UserError("Este documento ha sido informado a la SUNAT no se puede cambiar a borrador")
        return res
    
    @api.model
    def pe_credit_debit_code(self, invoice_ids, credit_code, debit_code):
        for invoice in self.browse(invoice_ids):
            if credit_code:
                invoice.c2u_debit_note_code= credit_code
            elif debit_code:
                invoice.c2u_debit_note_code = debit_code
    
    @api.multi
    def action_invoice_sent(self):
        res= super(AccountInvoice, self).action_invoice_sent()
        self.ensure_one()
        if self.journal_id.is_c2u and self.c2u_id:
            template = self.env.ref('l10n_pe_close2u.email_template_edi_invoice_c2u', False)
            attach={}
            attach['name']= self.c2u_invoice_fname
            attach['type']="binary"
            attach['datas']= self.c2u_invoice_datas
            attach['datas_fname']= self.c2u_invoice_fname
            attach['res_model']="mail.compose.message"
            attachment_id=self.env['ir.attachment'].create(attach)
            attachment_ids=[]
            attachment_ids.append(attachment_id.id)
            vals={}
            vals['default_use_template']=bool(template)
            vals['default_template_id']=template and template.id or False
            vals['default_attachment_ids']=[(6, 0, attachment_ids)]
            res['context'].update(vals)
        return  res    

    @api.onchange('partner_id', 'company_id')
    def _onchange_partner_id(self):
        res= super(AccountInvoice, self)._onchange_partner_id()
        if self.partner_id and self.journal_id.is_c2u:
            if self.partner_id.doc_type=="6":
                journal_id=self.env['account.journal'].search([('company_id','=',self.company_id.id), 
                                                               ('c2u_invoice_code', '=', 'FACTURA')], limit=1)
                if journal_id:
                    self.journal_id=journal_id.id
            else:
                journal_id=self.env['account.journal'].search([('company_id','=',self.company_id.id), 
                                                               ('c2u_invoice_code', '=', 'BOLETA')], limit=1)
                if journal_id:
                    self.journal_id=journal_id.id
        return res

    @api.multi
    def invoice_print(self):
        res=super(AccountInvoice, self).invoice_print()
        self.ensure_one()
        if self.is_c2u and self.c2u_invoice_datas:
            res= {
                    'type': 'ir.actions.act_url',
                    'url': '/web/content/account.invoice/%s/c2u_invoice_datas/%s' % (self.id, self.c2u_invoice_fname),
                    'target': 'new',
                }
        return res

class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'
    
    c2u_affectation_code = fields.Selection(selection= "_get_c2u_affectation_code", string="Type of affectation", 
                                           default="GRAVADO_OPERACION_ONEROSA", required=True, help="Type of affectation to the IGV")
    
    @api.model
    def _get_c2u_affectation_code(self):
        return self.env['pe.datas'].get_selection("PE.TIPO.AFECTACION")
    
    @api.onchange('c2u_affectation_code')
    def onchange_c2u_affectation_code(self):
        if self.c2u_affectation_code not in ['GRAVADO_OPERACION_ONEROSA', 'EXONERADO_OPERACION_ONEROSA', 
                                             'INAFECTO_OPERACION_ONEROSA', 'EXPORTACION']:
            if self.discount!=100:
               self.discount=100
        else:
            if self.discount==100:
               self.discount=0 
        if self.c2u_affectation_code not in ['GRAVADO_OPERACION_ONEROSA']:
            ids = self.invoice_line_tax_ids.ids
            igv_id= self.env['account.tax'].search([('c2u_tax_type', '=', '1000'), ('id', 'in', ids)])
            if igv_id:
                res= self.env['account.tax'].search([('id', 'in', ids), ('id', 'not in', igv_id.ids)]).ids
                self.invoice_line_tax_ids= [(6, 0, res)]
        else:
            ids = self.invoice_line_tax_ids.ids
            igv_id= self.env['account.tax'].search([('c2u_tax_type', '=', '1000'), ('id', 'in', ids)])
            if not igv_id:
                res= self.env['account.tax'].search([('c2u_tax_type', '=', '1000')])
                self.invoice_line_tax_ids= [(6, 0, ids+res.ids)]
    
    @api.multi
    def get_price_unit(self):
        self.ensure_one()
        price_unit = self.price_unit
        res = self.invoice_line_tax_ids.compute_all(price_unit, self.invoice_id.currency_id, 1, self.product_id, self.invoice_id.partner_id)
        return res
    