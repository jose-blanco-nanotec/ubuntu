# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class Company(models.Model):
    _inherit = "res.company"

    c2u_is_sync = fields.Boolean("Is Synchronous", default= True)
    c2u_cpe_server_id = fields.Many2one(comodel_name="pe.server", string="Server", 
                                     domain="[('state','=','done')]")
    sunat_amount = fields.Float(string="Amount", digits=(16,2), default=700)