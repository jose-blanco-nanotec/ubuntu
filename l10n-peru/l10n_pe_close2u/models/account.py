# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class AccountJournal(models.Model):
    _inherit = "account.journal"

    is_c2u = fields.Boolean("Is Close2u")
    c2u_invoice_code = fields.Selection(selection="_get_pe_invoice_code", string="Invoice Type Code")
    
    @api.model
    def _get_pe_invoice_code(self):
        return self.env['pe.datas'].get_selection("PE.TIPO.DOCUMENTO")

class AccountTax(models.Model):
    _inherit = 'account.tax'
    
    c2u_tax_type = fields.Selection([('1000', 'IGV'), ('2000', 'ISC'), ('9999', 'OTROS')], "Tax Code")

class AccountPaymentTerm(models.Model):
    _inherit = 'account.payment.term'
    
    c2y_payment_method = fields.Selection("_get_c2y_payment_method", "Payment Method")
    
    @api.model
    def _get_c2y_payment_method(self):
        return self.env['pe.datas'].get_selection("PE.C2U.FORM.PAGO")