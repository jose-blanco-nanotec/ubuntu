# -*- encoding: utf-8 -*-
from odoo import api, fields, models, _

class Partner(models.Model):
    _inherit = 'res.partner'

    @api.multi
    def get_c2u_doc_type(self):
        self.ensure_one()
        res=False
        if self.doc_type=="0":
            res="DOC_TRIB_NO_DOM_SIN_RUC"
        elif self.doc_type=="1":
            res="DOC_NACIONAL_DE_IDENTIDAD"
        elif self.doc_type=="4":
            res="CARNET_DE_EXTRANJERIA"
        elif self.doc_type=="6":
            res="RUC"
        elif self.doc_type=="7":
            res="PASAPORTE"
        elif self.doc_type=="A":
            res="CED_DIPLOMATICA_IDENTIDAD"
        return  res
    