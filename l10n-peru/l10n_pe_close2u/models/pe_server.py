# -*- coding: utf-8 -*-

from odoo import models, fields, api

class pe_sunat_server(models.Model):
    _inherit = 'pe.server'

    email = fields.Char("Email", required=True)