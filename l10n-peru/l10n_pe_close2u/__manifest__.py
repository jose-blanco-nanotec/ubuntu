# -*- coding: utf-8 -*-
{
    'name': "l10n_pe_close2u",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "FLEXXOONE",
    'website': "http://www.flexxoone.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'account',
        'stock',
        'fleet',
        'account_cancel',
        'amount_to_text',
        'l10n_pe_vat',
        'l10n_pe_datas',
        'l10n_pe_server',
        'l10n_pe_invoice',
        ],

    # always loaded
    'data': [
        'wizard/account_eguide_wizard_view.xml',
        "security/close2u_security.xml",
        'security/ir.model.access.csv',
        'data/pe.datas.csv',
        'views/account_invoice_view.xml',
        'views/account_view.xml',
        'views/server_view.xml',
        'views/product_view.xml',
        'views/company_view.xml',
        'views/close2u_view.xml',
        'wizard/account_invoice_debit_view.xml',
        'data/pe_c2u_data.xml',
        'data/invoice_data_action.xml',
        'views/stock_view.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}