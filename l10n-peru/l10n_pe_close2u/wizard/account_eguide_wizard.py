# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError

class AccountEguideWizard(models.TransientModel):
    _name = "account.eguide.wizard"
    
    @api.model
    def _get_c2u_transport_mode(self):
        return self.env['pe.datas'].get_selection("PE.C2U.CATALOG18")
    
    c2u_is_eguide = fields.Boolean("Is EGuide", copy=False)
    c2u_eguide_generated = fields.Boolean("Is Generated", copy=False)
    
    c2u_gross_weight = fields.Float("Gross Weigh", digits=dp.get_precision('Product Unit of Measure'), copy=False)
    c2u_unit_quantity = fields.Integer("Unit Quantity", copy=False)
    c2u_transport_mode = fields.Selection(selection="_get_c2u_transport_mode", string="Transport Mode", copy=False)
    c2u_carrier_id = fields.Many2one(comodel_name="res.partner", string="Carrier", copy=False)
    c2u_fleet_ids = fields.One2many(comodel_name="account.fleet.line.wizard", inverse_name="picking_id", string="Fleet Private", copy=False)
    
    @api.model
    def default_get(self, fields_list):
        res = super(AccountEguideWizard, self).default_get(fields_list)
        invoice_ids = self.env['account.invoice'].browse(self.env.context.get('active_ids',[]))
        c2u_eguide_generated=False
        c2u_gross_weight=0
        c2u_unit_quantity=0
        c2u_transport_mode=False
        c2u_carrier_id=False
        c2u_fleet_ids=[]
        for invoice_id in invoice_ids:
            if not invoice_id.pe_stock_ids:
                raise UserError(_("No store related"))
            else:
                for stock_id in invoice_id.pe_stock_ids:
                    c2u_eguide_generated = stock_id.c2u_eguide_generated
                    c2u_gross_weight = stock_id.c2u_gross_weight
                    c2u_unit_quantity = stock_id.c2u_unit_quantity
                    c2u_transport_mode = stock_id.c2u_transport_mode
                    c2u_carrier_id = stock_id.c2u_carrier_id.id
                    for fleet in stock_id.c2u_fleet_ids:
                        val={}
                        val['name']=fleet.name
                        val['fleet_id']=fleet.fleet_id.id
                        val['driver_id']= fleet.driver_id.id
                        c2u_fleet_ids.append((0,None,val))
            if c2u_gross_weight==0 and c2u_unit_quantity==0:
                for line in invoice_id.invoice_line_ids:
                    if line.product_id:
                        c2u_gross_weight+=line.product_id.weight                
                        c2u_unit_quantity+=line.quantity
            if not c2u_eguide_generated:
                c2u_eguide_generated = invoice_id.c2u_is_eguide
        res['c2u_eguide_generated']=c2u_eguide_generated
        res['c2u_gross_weight']=c2u_gross_weight
        res['c2u_unit_quantity']=c2u_unit_quantity  
        res['c2u_transport_mode']=c2u_transport_mode
        res['c2u_carrier_id']=c2u_carrier_id
        res['c2u_fleet_ids']=c2u_fleet_ids
        return res
    
    @api.one
    def create_eguide(self):
        invoice_ids = self.env['account.invoice'].browse(self.env.context.get('active_ids',[]))
        for invoice_id in invoice_ids:
            for stock_id in invoice_id.pe_stock_ids:
                stock_id.c2u_is_eguide=True
                stock_id.c2u_eguide_generated = True
                stock_id.c2u_gross_weight = self.c2u_gross_weight
                stock_id.c2u_unit_quantity = self.c2u_unit_quantity 
                stock_id.c2u_transport_mode = self.c2u_transport_mode
                stock_id.c2u_carrier_id = self.c2u_carrier_id.id
                c2u_fleet_ids=[]
                stock_id.c2u_fleet_ids.unlink()
                for fleet in self.c2u_fleet_ids:
                    val={}
                    val['name']=fleet.name
                    val['fleet_id']=fleet.fleet_id.id
                    val['driver_id']= fleet.driver_id.id
                    c2u_fleet_ids.append((0,None,val))
                stock_id.c2u_fleet_ids = c2u_fleet_ids
            invoice_id.c2u_is_eguide = True
            invoice_id.c2u_operation_type = 'FACTURA_GUIA'
        
class AccountStockFleetWizard(models.TransientModel):
    _name = "account.fleet.line.wizard"
    
    name = fields.Char("License Plate", required=True)
    fleet_id = fields.Many2one(comodel_name="fleet.vehicle", string="Vehicle")
    picking_id = fields.Many2one(comodel_name="account.eguide.wizard", string="Picking")
    driver_id = fields.Many2one(comodel_name="res.partner", string="Driver", required=True)
    
    @api.onchange("fleet_id")
    def onchange_fleet_id(self):
        if self.fleet_id:
            self.name = self.fleet_id.license_plate
            self.driver_id = self.fleet_id.driver_id.id
    