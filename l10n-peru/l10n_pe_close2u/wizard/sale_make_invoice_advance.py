# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class SaleAdvancePaymentInv(models.TransientModel):
    _inherit = "sale.advance.payment.inv"

    @api.multi
    def create_invoices(self):
        order = self.env['sale.order'].browse(self._context.get('active_ids', []))
        if order.partner_id.doc_type=="6":
            journal=self.env['account.journal'].search([('c2u_invoice_code', '=', 'FACTURA')], limit=1)
            journal_id= journal and journal.id or False
        else:
            journal=self.env['account.journal'].search([('c2u_invoice_code', '=', 'BOLETA')], limit=1)
            journal_id= journal and journal.id or False
        if journal_id:
            self = self.with_context(default_journal_id=journal_id)
        res = super(SaleAdvancePaymentInv, self).create_invoices()
        return res
    