# -*- coding: utf-8 -*-
from odoo import models, fields, api, _

class AccountInvoiceRefund(models.TransientModel):
    _inherit = "account.invoice.refund"
    
    c2u_debit_note_code = fields.Selection(selection="_get_pe_debit_note_type", string="Dedit Note Code")
    c2u_credit_note_code = fields.Selection(selection="_get_pe_crebit_note_type", string="Credit Note Code")
    
    @api.model
    def _get_pe_crebit_note_type(self):
        return self.env['pe.datas'].get_selection("PE.NOTA.CREDITO")
    
    @api.model
    def _get_pe_debit_note_type(self):
        return self.env['pe.datas'].get_selection("PE.NOTA.DEBITO")
