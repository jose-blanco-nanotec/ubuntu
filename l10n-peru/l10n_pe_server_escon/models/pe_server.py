# -*- coding: utf-8 -*-

from odoo import models, fields, api

class pe_sunat_server(models.Model):
    _inherit = 'pe.server'
    url = fields.Char("Url para envio de comprobantes de pago")
    url_get_status_cdr = fields.Char("Url de consulta de CDR y estado de envío")
    url_get_status = fields.Char("Url para consultar el status del ticket")
    url_send_summary = fields.Char("Url para el envio de lotes o resumenes")