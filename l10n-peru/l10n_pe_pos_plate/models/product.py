from odoo import api, fields, models, tools, _

class ProductProduct(models.Model):
    _inherit = "product.product"

    require_plate = fields.Boolean("Require Plate", related="product_tmpl_id.require_plate")


class pos_config_code(models.Model):
    _inherit = 'pos.config' 

    show_barcode = fields.Boolean('show barcode invoice', default=True)
    show_internal = fields.Boolean('show barcode refencia interna', default=True)

