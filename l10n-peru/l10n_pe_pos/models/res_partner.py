# -*- coding: utf-8 -*-

from odoo import api, fields, models
from datetime import datetime
import requests

class ResPartner(models.Model):
    _inherit = 'res.partner'
    def buscar_mintra(self, nro_documento):
        str_parse = "|"
        reponse = False

        url = 'http://extranet.trabajo.gob.pe/extranet/web/citas/validarDNI'
        req = {'dni': nro_documento}
        try:
            reponse = requests.post(url, req, timeout=300)
        except Exception:
            reponse = False
        if reponse and reponse.status_code == 200 and reponse.text != "-2":
            print("respuesta:", reponse.text)
            str = reponse.text.split(str_parse)
            respuesta = {
                'detail': 'found',
                'paternal_surname': str[1],
                'maternal_surname': str[2],
                'name': str[3],
                'sexo': str[4],
                'fecha_nacimiento': str[5][:4] + '/' + str[5][4:6] + '/' + str[5][6:8]
            }
        else:
            respuesta = {'detail': "Not found."}
        return respuesta

    @api.multi
    def buscar_dni(self, nro_dni):
        respuesta = self.buscar_mintra(nro_dni)
        return respuesta

    @api.model
    def get_partner_from_ui(self, doc_type=None, doc_number=None):
        url=None
        res={}

        if doc_type=="1":
            #respuesta = self.buscar_dni()
            #self.name = "%s %s %s" % ((respuesta['name'] or ''), (respuesta['paternal_surname'] or ''),
            #                          (respuesta['maternal_surname'] or ''))
            #self.company_type = "person"
            #self.is_validate = True
            #self.vat = "%s%s" % ("PED", vat)
            url = "http://api.grupoyacck.com/dni/%s/" % doc_number
        elif doc_type=="6":
            url = "http://api.grupoyacck.com/ruc/%s/?force_update=1" % doc_number
        if url:
            try:
                response = requests.get(url)
            except Exception:
                reponse=False
            if response and response.status_code==200:
                vals = response and response.json() or {'detail':"Not found."}
                print("vals:", vals)
                res = vals
                print("res:", res)
        return res
    
    @api.model
    def create_from_ui(self, partner):
        if partner.get('last_update',False):
            last_update = partner.get('last_update',False)
            if len(last_update)==27:
                partner['last_update'] = fields.Datetime.context_timestamp(self, datetime.strptime(partner.get('last_update'), '%Y-%m-%dT%H:%M:%S.%fZ')) or False
        if partner.get('is_validate',False):
            if partner.get('is_validate',False)=='true':
                partner['is_validate'] = True
            else:
                partner['is_validate'] = False 
        if not partner.get('state',False):
            partner['state'] = 'ACTIVO'
        if not partner.get('condition',False):
            partner['condition'] = 'HABIDO'
        if partner.get('doc_type',False) and partner.get('doc_type',False)==6:
            partner['company_type']="company"
        res = super(ResPartner, self).create_from_ui(partner)
        #self.browse(res).update_document()
        return res
