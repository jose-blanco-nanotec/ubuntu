# -*- coding: utf-8 -*-
{
    'name': "Peruvian Electronic Voucher for ESCONCORP using REST",

    'summary': """
        Peruvian Electronic Payment Voucher for ESCONCORP using REST
    """,

    'description': """
        Peruvian Electronic Payment Voucher for ESCONCORP using REST
    """,

    'author': "FLEXXOONE",
    'website': "http://www.flexxoone.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.2',

    # any module necessary for this one to work correctly
    'depends': [
        'l10n_pe_cpe',
    ],

    # always loaded
    'data': [
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}