# -*- coding: utf-8 -*-
from lxml import etree
from io import StringIO, BytesIO
import xmlsec
from collections import OrderedDict
from pysimplesoap.client import SoapClient, SoapFault, fetch
import base64, zipfile
from base64 import b64decode, b64encode
import xmltodict
import json
import requests
from odoo import _
from odoo.exceptions import UserError

from datetime import datetime
from pysimplesoap.simplexml import SimpleXMLElement
import logging
from odoo.tools import float_is_zero, float_round
from tempfile import gettempdir
import socket
from binascii import hexlify
log = logging.getLogger(__name__)

class Client(object):

    def __init__(self, ruc, username, password, url, debug=False, type=None):
        self._type = type
        self._username = '%s%s' % (ruc, username)
        self._password = password
        self._debug = debug
        self._url = url
        self._filename = None
        self._fileContent = None
        self._cadena_rest = '{"customer": {"username": %s, \n "password": %s }, "fileName": %s, "fileContent": %s }'
        self._xml_method = None
        self._exceptions = True
        level = logging.DEBUG
        logging.basicConfig(level=level)
        log.setLevel(level)
        self._call_service_rest('envio')

    def _connect(self):
        if not self._type:
            cache = '%s/sunat' % gettempdir()


    def _call_service_rest(self, name):
        respuesta = False
        headers = {'Content-type': 'application/json',}
        trama = self._cadena_rest
        trama%(self._username, self._password, self._filename, self._fileContent)
        if name == 'envio':
            respuesta = requests.post(self._url, data = trama, headers = headers)
            print("resp ", respuesta.code_status)

    def _call_service(self, name, params):
        if not self._type:
            try:
                service = getattr(self._client, name)
                return (
                    True, service(**params))
            except SoapFault as ex:
                return (
                    False, {'faultcode': ex.faultcode, 'faultstring': ex.faultstring})

        try:
            xml = self._restenv % (self._username, self._password, self._xml_method)
            return self._call_ws(xml)
            state = True
        except Exception as e:
            return (
                False, {})

    def send_bill(self, filename, content_file):
        params = {'fileName': filename, 'contentFile': str(content_file, 'utf-8')}
        self._contenido = '"fileName": %s, "fileContent": %s' % (params['fileName'], params['contentFile'])
        return self._call_service('envio', params)

    def send_summary(self, filename, content_file):
        params = {'fileName': filename,
                  'contentFile': str(content_file, 'utf-8')}
        self._xml_method = '<tzmed:sendSummary>\n            <fileName>%s</fileName>\n            <contentFile>%s</contentFile>\n        </tzmed:sendSummary>' % (
        params['fileName'], params['contentFile'])
        return self._call_service('sendSummary', params)

    def get_status(self, ticket_code):
        params = {'ticket': ticket_code}
        self._xml_method = '<tzmed:getStatus>\n            <ticket>%s</ticket>\n        </tzmed:getStatus>' % params[
            'ticket']
        return self._call_service('getStatus', params)

    def get_status_cdr(self, document_name):
        res = document_name.split('-')
        params = {'rucComprobante': res[0],
                  'tipoComprobante': res[1],
                  'serieComprobante': res[2],
                  'numeroComprobante': res[3]}
        self._xml_method = '<tzmed:getStatusCdr>\n            <rucComprobante>%s</rucComprobante>\n            <tipoComprobante>%s</tipoComprobante>\n            <serieComprobante>%s</serieComprobante>\n            <numeroComprobante>%s</numeroComprobante>\n        </tzmed:getStatusCdr>' % (
        params['rucComprobante'], params['tipoComprobante'], params['serieComprobante'],
        params['numeroComprobante'])
        return self._call_service('getStatusCdr', params)



class Document(object):

    def __init__(self):
        self._xml = None
        self._json = None
        self._type = None
        self._document_name = None
        self._client = None
        self._response = None
        self._response_status = None
        self._response_data = None
        self._ticket = None
        self.in_memory_data = BytesIO()
        self._contenido_filename = BytesIO()

        def send(self):
            if self._type == 'sync':

                self._response_status, self._response = self._client.send_envio(self.in_memory_data, self._document_name)
            '''
            else:
                if self._type == 'ticket':
                    self._response_status, self._response = self._client.get_status(self._ticket)
                else:
                    if self._type == 'status':
                        self._response_status, self._response = self._client.get_status_cdr(self._document_name)
                    else:
                        self._zip_file = base64.b64encode(self.in_memory_data.getvalue())
                        self._response_status, self._response = self._client.send_summary(self._zip_filename,
                                                                                                 self._zip_file)
            '''
    def process(self, document_name, type, xml, client):
        self._xml = xml
        self._type = type
        self._document_name = document_name
        self._client = client
        self.preparar_archivo()
        self.send()
        self.process_response()
        return (False, self._response_status, self._response, self._response_data)

    def preparar_archivo(self):
        self._contenido_filename = ('{}.json').format(self._document_name)
        json_filename =  ('{}.json').format(self._document_name)
        self.writetofile(json_filename, self._json)

    def process_response(self):
        if self._response is not None and self._response_status and self._type == 'sync':
            self._response_data = self._response['applicationResponse']
        else:
            if self._response is not None and self._response_status and self._type == 'ticket':
                if self._response.get('status', {}).get('content'):
                    self._response_data = self._response['status']['content']
                else:
                    res = self._response
                    self._response_status = False
                    self._response = {'faultcode': res['status'].get('statusCode', False), 'faultstring': ''}
            else:
                if self._response is not None and self._response_status and self._type == 'status':
                    self._response_data = self._response.get('statusCdr', {}).get('content', None)
                    if not self._response_data:
                        self._response_status = False
                        self._response = {'faultcode': self._response.get('statusCdr', {}).get('statusCode', False),
                                          'faultstring': self._response.get('statusCdr', {}).get('statusMessage',
                                                                                                 False)}
                else:
                    if self._response is not None and self._response_status:
                        self._response_data = self._response['ticket']

    @staticmethod
    def get_response(file, name):
        zf = BytesIO(base64.b64decode(file))
        return zf.open(name).read()

    def get_status(self, ticket, client):
        self._type = 'ticket'
        self._ticket = ticket
        self._client = client
        self.send()
        self.process_response()
        return (
            self._response_status, self._response, self._response_data)

    def get_status_cdr(self, document_name, client):
        self._type = 'status'
        self._client = client
        self._document_name = document_name
        self.send()
        self.process_response()
        return (
            self._response_status, self._response, self._response_data)


class CPE:
    def getIDE(invoice_id):
        item = {}
        item['numeracion'] = invoice_id.number
        item['fechaEmision'] = datetime.strptime(invoice_id.pe_invoice_date, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
        item['horaEmision'] = datetime.strptime(invoice_id.pe_invoice_date, '%Y-%m-%d %H:%M:%S').strftime('%H:%M:%S')
        item['codTipoDocumento'] = invoice_id.pe_invoice_code
        item['tipoMoneda'] = invoice_id.currency_id.name
        item['numeroOrdenCompra'] =  (invoice_id.name or '')
        return item

    def getEMI(invoice_id):
        item = {}
        item['tipoDocId'] = invoice_id.company_id.partner_id.doc_type or ''
        item['numeroDocId'] = invoice_id.company_id.partner_id.commercial_name if invoice_id.company_id.partner_id.commercial_name != '-' else invoice_id.company_id.partner_id.name or ''
        item['nombreComercial'] = invoice_id.company_id.partner_id.commercial_name if invoice_id.company_id.partner_id.commercial_name != '-' else invoice_id.company_id.partner_id.name or ''
        item['razonSocial'] = invoice_id.company_id.partner_id.legal_name if invoice_id.company_id.partner_id.legal_name != '-' else invoice_id.company_id.partner_id.name or ''
        item['ubigeo'] = invoice_id.company_id.partner_id.district_id.code[6:]
        item['direccion'] = invoice_id.company_id.partner_id.street[:200]
        item['urbanizacion'] = ''
        item['departamento'] = invoice_id.company_id.partner_id.state_id.name or ''
        item['provincia'] = invoice_id.company_id.partner_id.province_id.name or ''
        item['distrito'] = invoice_id.company_id.partner_id.district_id.name or ''
        item['codigoPais'] = 'PE'
        item['telefono'] = invoice_id.company_id.partner_id.phone or ''
        item['correoElectronico'] = invoice_id.company_id.partner_id.email or ''
        item['codigoAsigSUNAT'] = '0000'

        return item

    def getREC(invoice_id):
        item = {}
        item['tipoDocId'] = invoice_id.partner_id.doc_type
        item['numeroDocId'] = invoice_id.partner_id.doc_number
        item['razonSocial'] = invoice_id.partner_id.legal_name if invoice_id.partner_id.legal_name != '-' else invoice_id.partner_id.name or ''
        item['direccion'] = invoice_id.partner_id.street[:200] or ''
        item['departamento'] = invoice_id.partner_id.state_id.name or ''
        item['provincia'] = invoice_id.partner_id.province_id.name or ''
        item['distrito'] = invoice_id.partner_id.district_id.name or ''
        item['codigoPais'] = 'PE'
        item['telefono'] = invoice_id.partner_id.phone or ''
        item['correoElectronico'] = invoice_id.partner_id.email or ''

        return item

    def getDRF(invoice_id):
        base_drf = []
        item = {}
        item['tipoDocRelacionado'] = ''
        item['numeroDocRelacionado'] = ''

        base_drf.append(item)

        return base_drf

    def getCAB(invoice_id):
        #ubicando el tipo de factura
        list_cab = {}
        es_grabada = True
        if es_grabada:
            item_grabadas = {}


            amount_tax_1000 = 0.0
            amount_base_1000 = 0.0
            amount_base = 0.0
            amount_tax = 0.0
            str_monto = False

            for line in invoice_id.pe_additional_property_ids:
                str_monto = line.code

            line_ids = invoice_id.invoice_line_ids.filtered(
                lambda ln: ln.pe_affectation_code not in ('10', '20', '30', '40'))
            tax_ids = line_ids.mapped('invoice_line_tax_ids')

            for tax in tax_ids.filtered(lambda tax: tax.pe_tax_type.code != '9996'):
                base_amount = 0.0
                tax_amount = 0.0
                for line in line_ids:
                    price_unit = line.price_unit
                    if tax.id in line.invoice_line_tax_ids.ids:
                        tax_values = tax.with_context(round=False).compute_all(price_unit,
                                                                               currency=invoice_id.currency_id,
                                                                               quantity=line.quantity,
                                                                               product=line.product_id,
                                                                               partner=invoice_id.partner_id)
                        base_amount += invoice_id.currency_id.round(tax_values['total_excluded'])
                        tax_amount += invoice_id.currency_id.round(tax_values['taxes'][0]['amount'])

            item_grabadas['codigo'] = '1001'
            item_grabadas['totalVentas'] = str(round(amount_base, 2))
            list_cab['grabada'] = item_grabadas


            list_cab['totalImpuestos']=[]
            for tax in invoice_id.tax_line_ids.filtered(lambda tax: tax.tax_id.pe_tax_type.code != '9996'):
                if tax.tax_id.pe_tax_type.code == '1000':
                    item_total_impuesto = {}
                    amount_base_1000 += tax.base
                    amount_tax_1000 += tax.amount_total

                    item_total_impuesto['totalImpuestos'] = '10001'
                    item_total_impuesto['montoImpuesto'] = str(round(amount_base, 2))

                    list_cab['totalImpuestos'].append(item_total_impuesto)

            list_cab['importeTotal'] = str(round(invoice_id.amount_total, 2))
            list_cab['tipoOperacion'] = invoice_id.pe_sunat_transaction51

            list_cab['leyenda'] = []


            for line in invoice_id.pe_additional_property_ids:
                item_leyenda = {}
                item_leyenda['codigo'] = '1000'
                item_leyenda['descripcion'] = line.value

                list_cab['leyenda'].append(item_leyenda)

            list_cab['montoTotalImpuestos'] = str(round(amount_base, 2))

        return list_cab

    def getDET(invoice_id):
        line_det = []
        cont = 1
        decimal_precision_obj = invoice_id.env['decimal.precision']
        digits = decimal_precision_obj.precision_get('Product Price') or 2

        for line in invoice_id.invoice_line_ids.filtered(lambda ln: ln.price_subtotal >= 0):
            line_det = {}
            price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            taxes = line.invoice_line_tax_ids.with_context(round=False).compute_all(price_unit,
                                                                                    currency=invoice_id.currency_id,
                                                                                    quantity=line.quantity,
                                                                                    product=line.product_id,
                                                                                    partner=invoice_id.partner_id)
            tax_total_amount = taxes.get('total_included', 0.0) - taxes.get('total_excluded', 0.0)
            line_det['numeroItem'] = str(cont)
            line_det['codigoProducto'] = str(line.product_id.barcode or '')
            line_det['descripcionProducto'] = line.name.replace('\n', ' ')[:250]
            line_det['cantidadItems'] = str(line.quantity)
            line_det['unidad'] = str(line.uom_id and line.uom_id.sunat_code or 'NIU')
            line_det['precioVentaUnitario'] = str(round(line.price_unit, 2))

            price_unit_all = line.get_price_unit(True)['total_included']

            if price_unit_all > 0:
                print('<--->')

            if price_unit_all == 0 or invoice_id.pe_invoice_code in ('07', '08'):
                line_det['valorUnitario'] = str(round(float_round(price_unit_all, digits), 10))
            else:
                line_det['valorUnitario'] = str(round(float_round(price_unit_all, digits), 10))

            if price_unit_all == 0.0:
                """
                tag = etree.QName(self._cac, 'AlternativeConditionPrice')
                alternative = etree.SubElement(pricing, tag.text, nsmap={'cac': tag.namespace})
                tag = etree.QName(self._cbc, 'PriceAmount')
                etree.SubElement(alternative, tag.text, currencyID=invoice_id.currency_id.name,
                                 nsmap={'cbc': tag.namespace}).text = str(
                    round(float_round(line.get_price_unit()['total_included'], digits), 10))
                tag = etree.QName(self._cbc, 'PriceTypeCode')
                etree.SubElement(alternative, tag.text, listName='Tipo de Precio', listAgencyName='PE:SUNAT',
                                 listURI='urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo16',
                                 nsmap={'cbc': tag.namespace}).text = '02'
                """
            if line.discount > 0 and line.discount < 100 and invoice_id.pe_invoice_code not in ('07', '08'):
                """
                tag = etree.QName(self._cac, 'AllowanceCharge')
                charge = etree.SubElement(inv_line, tag.text, nsmap={'cac': tag.namespace})
                tag = etree.QName(self._cbc, 'ChargeIndicator')
                etree.SubElement(charge, tag.text, nsmap={'cbc': tag.namespace}).text = 'false'
                tag = etree.QName(self._cbc, 'AllowanceChargeReasonCode')
                etree.SubElement(charge, tag.text, nsmap={'cbc': tag.namespace}).text = '00'
                tag = etree.QName(self._cbc, 'MultiplierFactorNumeric')
                etree.SubElement(charge, tag.text, nsmap={'cbc': tag.namespace}).text = str(
                    round(line.discount / 100, 5))
                tag = etree.QName(self._cbc, 'Amount')
                etree.SubElement(charge, tag.text, currencyID=invoice_id.currency_id.name,
                                 nsmap={'cbc': tag.namespace}).text = str(
                    round(float_round(line.discount == 100 and 0.0 or line.amount_discount, digits), 10))
                tag = etree.QName(self._cbc, 'BaseAmount')
                etree.SubElement(charge, tag.text, currencyID=invoice_id.currency_id.name,
                                 nsmap={'cbc': tag.namespace}).text = str(
                    round(float_round(line.price_subtotal + line.amount_discount, digits), 10))
                """

            tax_total_amount = taxes.get('total_included', 0.0) - taxes.get('total_excluded', 0.0)
            digits_rounding_precision = invoice_id.currency_id.rounding

            if line.invoice_line_tax_ids.filtered(lambda tax: tax.pe_tax_type.code == '9996'):
                tax_total_values = line.invoice_line_tax_ids.with_context(round=False).filtered(
                    lambda tax: tax.pe_tax_type.code != '9996').compute_all(line.price_unit,
                                                                            currency=invoice_id.currency_id,
                                                                            quantity=line.quantity,
                                                                            product=line.product_id,
                                                                            partner=invoice_id.partner_id)
            line_det['totalImpuestos'] = []
            for tax in line.invoice_line_tax_ids:
                item_tax = {}
                if tax.pe_tax_type.code == '9996':
                    price_unit = line.price_unit
                    tax_values = tax_total_values
                else:
                    if line.discount == 100:
                        continue
                    else:
                        tax_values = tax.with_context(round=False).compute_all(price_unit,
                                                                               currency=invoice_id.currency_id,
                                                                               quantity=line.quantity,
                                                                               product=line.product_id,
                                                                               partner=invoice_id.partner_id)
                    if float_is_zero(tax_values['total_excluded'],
                                     precision_rounding=digits_rounding_precision) and tax.pe_tax_type.code == '9996' or not float_is_zero(
                            tax_values['total_excluded'], precision_rounding=digits_rounding_precision):
                        item_tax['idImpuesto']= tax.pe_tax_type.code
                        item_tax['montoImpuesto'] = str(round(float_round(tax_values['taxes'][0]['amount'], precision_rounding=digits_rounding_precision), 2))
                        item_tax['tipoAfectacion'] = line.pe_affectation_code
                        item_tax['montoBase'] = str(round(float_round(tax_values['total_excluded'], precision_rounding=digits_rounding_precision), 2))

                        taxes_ids = line.invoice_line_tax_ids.filtered(lambda tax: tax.pe_tax_type.code != '9996')
                        amount = tax.pe_tax_type.code == '9996' and (
                                    len(taxes_ids) > 1 and taxes_ids[0].amount or taxes_ids.amount) or tax.amount
                        item_tax['porcentaje'] = str(amount)
                        line_det['totalImpuestos'].append(item_tax)

            line_det['valorVenta'] = str(round(float_round(price_unit_all, digits), 10))
            line_det['montoTotalImpuestos'] = str(round(float_round(tax_total_amount, digits), 10))

            cont += 1

        return line_det

    def getIDE_as(r_ide, invoice_id):
        tag = etree.QName('numeracion')
        etree.SubElement(r_ide, tag.text).text=invoice_id.number
        tag = etree.QName('fechaEmision')
        etree.SubElement(r_ide, tag.text).text = datetime.strptime(invoice_id.pe_invoice_date, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
        tag = etree.QName('horaEmision')
        etree.SubElement(r_ide, tag.text).text = datetime.strptime(invoice_id.pe_invoice_date, '%Y-%m-%d %H:%M:%S').strftime('%H:%M:%S')
        tag = etree.QName('codTipoDocumento')
        etree.SubElement(r_ide, tag.text).text = invoice_id.pe_invoice_code
        tag = etree.QName('tipoMoneda')
        etree.SubElement(r_ide, tag.text).text = invoice_id.currency_id.name
        tag = etree.QName('numeroOrdenCompra')
        etree.SubElement(r_ide, tag.text).text = (invoice_id.name or '')

    def getEMI_as(r_emi, invoice_id):
        tag = etree.QName('tipoDocId')
        etree.SubElement(r_emi, tag.text).text=invoice_id.company_id.partner_id.doc_type or ''
        tag = etree.QName('numeroDocId')
        etree.SubElement(r_emi, tag.text).text=invoice_id.company_id.partner_id.commercial_name if invoice_id.company_id.partner_id.commercial_name != '-' else invoice_id.company_id.partner_id.name or ''
        tag = etree.QName('nombreComercial')
        etree.SubElement(r_emi, tag.text).text=invoice_id.company_id.partner_id.commercial_name if invoice_id.company_id.partner_id.commercial_name != '-' else invoice_id.company_id.partner_id.name or ''
        tag = etree.QName('razonSocial')
        etree.SubElement(r_emi, tag.text).text=invoice_id.company_id.partner_id.legal_name if invoice_id.company_id.partner_id.legal_name != '-' else invoice_id.company_id.partner_id.name or ''
        tag = etree.QName('ubigeo')
        etree.SubElement(r_emi, tag.text).text=invoice_id.company_id.partner_id.district_id.code[6:]
        tag = etree.QName('direccion')
        etree.SubElement(r_emi, tag.text).text=invoice_id.company_id.partner_id.street[:200]
        tag = etree.QName('urbanizacion')
        etree.SubElement(r_emi, tag.text).text= ''
        tag = etree.QName('departamento')
        etree.SubElement(r_emi, tag.text).text=invoice_id.company_id.partner_id.state_id.name or ''
        tag = etree.QName('provincia')
        etree.SubElement(r_emi, tag.text).text=invoice_id.company_id.partner_id.province_id.name or ''
        tag = etree.QName('distrito')
        etree.SubElement(r_emi, tag.text).text=invoice_id.company_id.partner_id.district_id.name or ''
        tag = etree.QName('codigoPais')
        etree.SubElement(r_emi, tag.text).text='PE'
        tag = etree.QName('telefono')
        etree.SubElement(r_emi, tag.text).text=invoice_id.company_id.partner_id.phone or ''
        tag = etree.QName('correoElectronico')
        etree.SubElement(r_emi, tag.text).text=invoice_id.company_id.partner_id.email or ''
        tag = etree.QName('codigoAsigSUNAT')
        etree.SubElement(r_emi, tag.text).text= '0000'

    def getREC_xml(r_rec, invoice_id):
        tag = etree.QName('tipoDocId')
        etree.SubElement(r_rec, tag.text).text= invoice_id.partner_id.doc_type
        tag = etree.QName('numeroDocId')
        etree.SubElement(r_rec, tag.text).text= invoice_id.partner_id.doc_number
        tag = etree.QName('razonSocial')
        etree.SubElement(r_rec, tag.text).text= invoice_id.partner_id.legal_name if invoice_id.partner_id.legal_name != '-' else invoice_id.partner_id.name or ''
        tag = etree.QName('direccion')
        etree.SubElement(r_rec, tag.text).text= invoice_id.partner_id.street[:200] or ''
        tag = etree.QName('departamento')
        etree.SubElement(r_rec, tag.text).text= invoice_id.partner_id.state_id.name or ''
        tag = etree.QName('provincia')
        etree.SubElement(r_rec, tag.text).text= invoice_id.partner_id.province_id.name or ''
        tag = etree.QName('distrito')
        etree.SubElement(r_rec, tag.text).text= invoice_id.partner_id.district_id.name or ''
        tag = etree.QName('codigoPais')
        etree.SubElement(r_rec, tag.text).text= 'PE'
        tag = etree.QName('telefono')
        etree.SubElement(r_rec, tag.text).text= invoice_id.partner_id.phone or ''
        tag = etree.QName('correoElectronico')
        etree.SubElement(r_rec, tag.text).text= invoice_id.partner_id.email or ''

    def getDRF_xml(r_drf, invoice_id):
        tag = etree.QName('tipoDocRelacionado')
        etree.SubElement(r_drf, tag.text).text= ''
        tag = etree.QName('numeroDocRelacionado')
        etree.SubElement(r_drf, tag.text).text= ''

    def getCAB_xml(r_cab, invoice_id):
        amount_tax_1000 = 0.0
        amount_base_1000 = 0.0
        amount_base = 0.0
        amount_tax = 0.0
        str_monto = False

        for line in invoice_id.pe_additional_property_ids:
            str_monto = line.code

        line_ids = invoice_id.invoice_line_ids.filtered(
            lambda ln: ln.pe_affectation_code not in ('10', '20', '30', '40'))
        tax_ids = line_ids.mapped('invoice_line_tax_ids')

        for tax in tax_ids.filtered(lambda tax: tax.pe_tax_type.code != '9996'):
            base_amount = 0.0
            tax_amount = 0.0
            for line in line_ids:
                price_unit = line.price_unit
                if tax.id in line.invoice_line_tax_ids.ids:
                    tax_values = tax.with_context(round=False).compute_all(price_unit,
                                                                           currency=invoice_id.currency_id,
                                                                           quantity=line.quantity,
                                                                           product=line.product_id,
                                                                           partner=invoice_id.partner_id)
                    base_amount += invoice_id.currency_id.round(tax_values['total_excluded'])
                    tax_amount += invoice_id.currency_id.round(tax_values['taxes'][0]['amount'])

        tag = etree.QName('gravadas')
        gravada = etree.SubElement(r_cab, tag.text)

        tag = etree.QName('codigo')
        etree.SubElement(gravada, tag.text).text = '1001'

        tag = etree.QName('totalVentas')
        etree.SubElement(gravada, tag.text).text = str(round(amount_base, 2))

        tag = etree.QName('totalImpuestos')
        total_impuestos = etree.SubElement(r_cab, tag.text)


        for tax in invoice_id.tax_line_ids.filtered(lambda tax: tax.tax_id.pe_tax_type.code != '9996'):
            if tax.tax_id.pe_tax_type.code == '1000':
                amount_base_1000 += tax.base
                amount_tax_1000 += tax.amount_total
                    
                tag = etree.QName('idImpuesto')
                etree.SubElement(total_impuestos, tag.text).text = '10001'

                tag = etree.QName('montoImpuesto')
                etree.SubElement(total_impuestos, tag.text).text = str(round(amount_base, 2))

        tag = etree.QName('importeTotal')
        etree.SubElement(r_cab, tag.text).text = str(round(invoice_id.amount_total,2))

        tag = etree.QName('tipoOperacion')
        etree.SubElement(r_cab, tag.text).text = invoice_id.pe_sunat_transaction51

        tag = etree.QName('leyenda')
        leyenda = etree.SubElement(r_cab, tag.text)

        for line in invoice_id.pe_additional_property_ids:
            tag = etree.QName('codigo')
            etree.SubElement(leyenda, tag.text).text = '1000'

            tag = etree.QName('descripcion')
            etree.SubElement(leyenda, tag.text).text = line.value

        tag = etree.QName('montoTotalImpuestos')
        etree.SubElement(r_cab, tag.text).text = str(round(amount_base, 2))

    def getDET_xml(r_det, invoice_id):
        str_json = []
        cont = 1
        decimal_precision_obj = invoice_id.env['decimal.precision']
        digits = decimal_precision_obj.precision_get('Product Price') or 2

        for line in invoice_id.invoice_line_ids.filtered(lambda ln: ln.price_subtotal >= 0):
            price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            taxes = line.invoice_line_tax_ids.with_context(round=False).compute_all(price_unit, currency=invoice_id.currency_id, quantity=line.quantity, product=line.product_id, partner=invoice_id.partner_id)
            tax_total_amount = taxes.get('total_included', 0.0) - taxes.get('total_excluded', 0.0)

            tag = etree.QName('numeroItem')
            etree.SubElement(r_det, tag.text).text = str(cont)
            tag = etree.QName('codigoProducto')
            etree.SubElement(r_det, tag.text).text = str(line.product_id.barcode)
            tag = etree.QName('descripcionProducto')
            etree.SubElement(r_det, tag.text).text = line.name.replace('\n', ' ')[:250]
            tag = etree.QName('cantidadItems')
            etree.SubElement(r_det, tag.text).text = str(line.quantity)
            tag = etree.QName('unidad')
            etree.SubElement(r_det, tag.text).text = str(line.uom_id and line.uom_id.sunat_code or 'NIU')
            tag = etree.QName('precioVentaUnitario')
            etree.SubElement(r_det, tag.text).text=str(round(line.price_unit,2))
            tag = etree.QName('totalImpuestos')
            total_impuesto = etree.SubElement(r_det, tag.text) #.text = str(round(tax_total_amount, 2))

            price_unit_all = line.get_price_unit(True)['total_included']

            if price_unit_all > 0:
                print('<--->')

            if price_unit_all == 0 or invoice_id.pe_invoice_code in ('07', '08'):
                tag = etree.QName('valorUnitario')
                etree.SubElement(r_det, tag.text).text = str(round(float_round(price_unit_all, digits), 10))
            else:
                tag = etree.QName('valorUnitario')
                etree.SubElement(r_det, tag.text).text = str(round(float_round(price_unit_all, digits), 10))

            if price_unit_all == 0.0:
                """
                tag = etree.QName(self._cac, 'AlternativeConditionPrice')
                alternative = etree.SubElement(pricing, tag.text, nsmap={'cac': tag.namespace})
                tag = etree.QName(self._cbc, 'PriceAmount')
                etree.SubElement(alternative, tag.text, currencyID=invoice_id.currency_id.name,
                                 nsmap={'cbc': tag.namespace}).text = str(
                    round(float_round(line.get_price_unit()['total_included'], digits), 10))
                tag = etree.QName(self._cbc, 'PriceTypeCode')
                etree.SubElement(alternative, tag.text, listName='Tipo de Precio', listAgencyName='PE:SUNAT',
                                 listURI='urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo16',
                                 nsmap={'cbc': tag.namespace}).text = '02'
                """
            if line.discount > 0 and line.discount < 100 and invoice_id.pe_invoice_code not in ('07', '08'):
                """
                tag = etree.QName(self._cac, 'AllowanceCharge')
                charge = etree.SubElement(inv_line, tag.text, nsmap={'cac': tag.namespace})
                tag = etree.QName(self._cbc, 'ChargeIndicator')
                etree.SubElement(charge, tag.text, nsmap={'cbc': tag.namespace}).text = 'false'
                tag = etree.QName(self._cbc, 'AllowanceChargeReasonCode')
                etree.SubElement(charge, tag.text, nsmap={'cbc': tag.namespace}).text = '00'
                tag = etree.QName(self._cbc, 'MultiplierFactorNumeric')
                etree.SubElement(charge, tag.text, nsmap={'cbc': tag.namespace}).text = str(
                    round(line.discount / 100, 5))
                tag = etree.QName(self._cbc, 'Amount')
                etree.SubElement(charge, tag.text, currencyID=invoice_id.currency_id.name,
                                 nsmap={'cbc': tag.namespace}).text = str(
                    round(float_round(line.discount == 100 and 0.0 or line.amount_discount, digits), 10))
                tag = etree.QName(self._cbc, 'BaseAmount')
                etree.SubElement(charge, tag.text, currencyID=invoice_id.currency_id.name,
                                 nsmap={'cbc': tag.namespace}).text = str(
                    round(float_round(line.price_subtotal + line.amount_discount, digits), 10))
                """

            tax_total_amount = taxes.get('total_included', 0.0) - taxes.get('total_excluded', 0.0)
            digits_rounding_precision = invoice_id.currency_id.rounding

            if line.invoice_line_tax_ids.filtered(lambda tax: tax.pe_tax_type.code == '9996'):
                tax_total_values = line.invoice_line_tax_ids.with_context(round=False).filtered(
                    lambda tax: tax.pe_tax_type.code != '9996').compute_all(line.price_unit,
                                                                            currency=invoice_id.currency_id,
                                                                            quantity=line.quantity,
                                                                            product=line.product_id,
                                                                            partner=invoice_id.partner_id)

            for tax in line.invoice_line_tax_ids:
                json_tax = []
                if tax.pe_tax_type.code == '9996':
                    price_unit = line.price_unit
                    tax_values = tax_total_values
                else:
                    if line.discount == 100:
                        continue
                    else:
                        tax_values = tax.with_context(round=False).compute_all(price_unit,
                                                                               currency=invoice_id.currency_id,
                                                                               quantity=line.quantity,
                                                                               product=line.product_id,
                                                                               partner=invoice_id.partner_id)
                    if float_is_zero(tax_values['total_excluded'], precision_rounding=digits_rounding_precision) and tax.pe_tax_type.code == '9996' or not float_is_zero(tax_values['total_excluded'], precision_rounding=digits_rounding_precision):
                        tag = etree.QName('idImpuesto')
                        etree.SubElement(total_impuesto, tag.text).text = tax.pe_tax_type.code
                        tag = etree.QName('montoImpuesto')
                        etree.SubElement(total_impuesto, tag.text).text = str(round(float_round(tax_values['taxes'][0]['amount'], precision_rounding=digits_rounding_precision), 2))
                        tag = etree.QName('tipoAfectacion')
                        etree.SubElement(total_impuesto, tag.text).text = line.pe_affectation_code
                        tag = etree.QName('montoBase')
                        etree.SubElement(total_impuesto, tag.text).text = str(round(float_round(tax_values['total_excluded'], precision_rounding=digits_rounding_precision), 2))

                        taxes_ids = line.invoice_line_tax_ids.filtered(lambda tax: tax.pe_tax_type.code != '9996')
                        amount = tax.pe_tax_type.code == '9996' and (len(taxes_ids) > 1 and taxes_ids[0].amount or taxes_ids.amount) or tax.amount

                        tag = etree.QName('porcentaje')
                        etree.SubElement(total_impuesto, tag.text).text = str(amount)
            tag = etree.QName('valorVenta')
            etree.SubElement(total_impuesto, tag.text).text = str(round(float_round(price_unit_all, digits), 10))
            tag = etree.QName('montoTotalImpuestos')
            etree.SubElement(total_impuesto, tag.text).text = str(round(float_round(tax_total_amount, digits), 10))
                        
            cont += 1

        return str_json

    def getADI(invoice_id):
        base_adi = []
        item = {}

        item['tituloAdicional'] = 'Parametro adicional Uno'
        item['valorAdicional'] = 'adicional Uno'
        base_adi.append(item)

        item['tituloAdicional'] = 'Parametro adicional Dos'
        item['valorAdicional'] = 'adicional Dos'
        base_adi.append(item)

        return base_adi

    def getADI_XML(r_adi, invoice_id):

        tag = etree.QName('tituloAdicional')
        etree.SubElement(r_adi, tag.text).text = 'parametro adicional uno'
        tag = etree.QName('valorAdicional')
        etree.SubElement(r_adi, tag.text).text = 'valor parametro uno'


        #tag = etree.QName('tituloAdicional')
        #etree.SubElement(r_adi, tag.text).text = 'Otro Parametro Adicional'
        #tag = etree.QName('valorAdicional')
        #etree.SubElement(r_adi, tag.text).text = '65-999999'

    def getInvoice(self, invoice_id):
        invoice = {}
        item = {}
        item['IDE'] = CPE.getIDE(invoice_id)
        item['EMI'] = CPE.getEMI(invoice_id)
        item['REC'] = CPE.getREC(invoice_id)
        item['DRF'] = CPE.getDRF(invoice_id)
        item['CAB'] = CPE.getCAB(invoice_id)
        item['DET'] = CPE.getDET(invoice_id)
        item['ADI'] = CPE.getADI(invoice_id)
        invoice['factura'] = item

        return json.dumps(invoice)

    def getInvoice_xml(self, invoice_id):
        inicio = etree.Element("factura")
        tag = etree.QName("IDE")
        ide = etree.SubElement(inicio, tag)
        CPE.getIDE(ide, invoice_id)

        tag = etree.QName("EMI")
        emi = etree.SubElement(inicio, tag)
        CPE.getEMI(emi, invoice_id)

        tag = etree.QName("REC")
        rec = etree.SubElement(inicio, tag)
        CPE.getREC(rec, invoice_id)

        tag = etree.QName("DRF")
        drf = etree.SubElement(inicio, tag)
        CPE.getDRF(drf, invoice_id)

        tag = etree.QName("CAB")
        cab = etree.SubElement(inicio, tag)
        CPE.getCAB(cab, invoice_id)

        tag = etree.QName("DET")
        det = etree.SubElement(inicio, tag)
        CPE.getDET(det, invoice_id)

        tag = etree.QName("ADI")
        adi = etree.SubElement(inicio, tag)
        CPE.getADI(adi, invoice_id)

        str_json = etree.tostring(inicio, pretty_print=True, xml_declaration=True, encoding='utf-8',
                                 standalone=False)

        data = json.dumps(xmltodict.parse(str_json))
        return data

    def get_response(data):
        return Document().get_response(**data)

    def send_sunat_cpe(client, document):
        client['type'] = 'send'
        client = Client(**client)
        document['client'] = client
        return Document().process(**document)

def get_document_invoice(self):
    json = None
    invoice_id = self.invoice_ids[0]
    if invoice_id.pe_invoice_code == '08':
        json = CPE.getInvoice(self, invoice_id)
    elif invoice_id.pe_invoice_code == '07':
        json = CPE.getInvoice(self, invoice_id)
    elif invoice_id.pe_invoice_code == '01':
        json = CPE.getInvoice(self, invoice_id)
    return json

def envioWS(self):
    ENCODING = 'utf-8'
    fileContent64 = str(b64encode(bytes(self.xml_document,ENCODING)), ENCODING)
    print('xml_document:', self.xml_document)
    cabecera = {'Content-Type': 'application/json', 'Accept': 'application/json'}
    url = "http://calidad.escondatagate.net/wsParser_2_1/rest/parserWS"
    usuario = self.company_id.pe_cpe_server_id.user
    password = self.company_id.pe_cpe_server_id.password
    fileName = self.datas_sign_fname
    ws_envio = '{"customer": {"username": "%s", "password": "%s"}, "fileName": "%s", "fileContent": "%s"}'%(usuario, password, fileName, fileContent64)
    request = requests.post(url, headers=cabecera, data=ws_envio)

    print('resp: ', request.status_code)
    print('resp: ', request.text)
    return request.status_code, request.text, request