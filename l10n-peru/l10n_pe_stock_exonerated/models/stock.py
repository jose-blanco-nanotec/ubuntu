# -*- coding: utf-8 -*-

from odoo import models, fields, api

class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.multi
    def _get_price_unit(self):
        self.ensure_one()
        line = self.purchase_line_id
        if line.pe_has_refund:
            res = super(StockMove, self)._get_price_unit()
        else:
            if self.purchase_line_id and self.product_id.id == self.purchase_line_id.product_id.id:
                order = line.order_id
                price_unit = line.price_unit
                if line.taxes_id:
                    price_unit = line.taxes_id.with_context(round=False).compute_all(price_unit, currency=line.order_id.currency_id, quantity=1.0)['total_included']
                if line.product_uom.id != line.product_id.uom_id.id:
                    price_unit *= line.product_uom.factor / line.product_id.uom_id.factor
                if order.currency_id != order.company_id.currency_id:
                    price_unit = order.currency_id.compute(price_unit, order.company_id.currency_id, round=False)
                res = price_unit
        return res