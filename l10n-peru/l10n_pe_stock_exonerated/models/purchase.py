# -*- coding: utf-8 -*-

from odoo import models, fields, api

class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'
    
    pe_has_refund = fields.Boolean("Has a refund", default = False)
    
    @api.onchange('product_id')
    def onchange_product_id(self):
        res = super(PurchaseOrderLine, self).onchange_product_id()
        if not self.product_id:
            return res
        if self.product_id.pe_has_refund:
            self.pe_has_refund = True
    
    @api.multi
    def _get_stock_move_price_unit(self):
        #self.ensure_one()
        line = self[0]
        if line.pe_has_refund:
            res = super(PurchaseOrderLine, self)._get_stock_move_price_unit()
        else:
            order = line.order_id
            price_unit = line.price_unit
            if line.taxes_id:
                price_unit = line.taxes_id.with_context(round=False).compute_all(
                    price_unit, currency=line.order_id.currency_id, quantity=1.0, product=line.product_id, partner=line.order_id.partner_id
                )['total_included']
            if line.product_uom.id != line.product_id.uom_id.id:
                price_unit *= line.product_uom.factor / line.product_id.uom_id.factor
            if order.currency_id != order.company_id.currency_id:
                price_unit = order.currency_id.compute(price_unit, order.company_id.currency_id, round=False)
            res =  price_unit
        return res