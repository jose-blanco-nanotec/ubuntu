# -*- coding: utf-8 -*-
{
    'name': "Stock Exonated",

    'summary': """
        Sotck, Exonerated, Peruvian""",

    'description': """
        Añade el impuesto en la valoracion de inventario
    """,

    'author': "FLEXXOONE",
    'website': "http://www.flexxoone.com",

    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['purchase',
                'stock',
                'stock_account'],

    # always loaded
    'data': [
        'security/l10n_pe_stock_exonerated_security.xml',
        'views/purchase_views.xml',
        'views/product_view.xml',
    ],
}