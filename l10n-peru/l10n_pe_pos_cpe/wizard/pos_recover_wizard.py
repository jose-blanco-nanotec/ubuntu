# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
import json
from base64 import b64decode

class PePosRecoverWizard(models.TransientModel):
    _name = "pe.pos.recover.wizard"
    
    name = fields.Char("Number")
    fname = fields.Char("File Name")
    fdatas = fields.Binary("Json Invoice")
    
    @api.multi
    def check_invoice_number(self):
        self.ensure_one()
        if self.name:
            if self.env['account.invoice'].search([('move_name','=',self.name)]):
                raise ValidationError(_("It is not a json file"))
        return True
    
    @api.multi
    def get_fdatas(self):
        self.ensure_one()
        res = False
        if self.fdatas:
            try:
                res = json.loads(str(b64decode(self.fdatas),'utf-8'))
            except Exception:
                raise ValidationError(_("It is not a json file"))
        #else:
        #    raise ValidationError(_("There is no data to process"))
        return res
    
    @api.onchange('fdatas')
    def onchange_fdatas(self):
        res = self.get_fdatas()
        if res:
            self.name = res.get('number', False)
            self.check_invoice_number()
    
    @api.multi
    def action_view_pos_order(self, order_id):
        self.ensure_one()
        action = self.env.ref('point_of_sale.action_pos_pos_form').read()[0]
        if order_id:
            action['views'] = [(self.env.ref('point_of_sale.view_pos_pos_form').id, 'form')]
            action['res_id'] = order_id[0]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action
    
    @api.multi
    def create_order(self):
        self.ensure_one()
        res = self.get_fdatas()
        res['number'] = self.name
        self.check_invoice_number()
        orders = [{'id':res['uid'],'data':res}]
        order_id = self.env['pos.order'].create_from_ui(orders)
        return self.action_view_pos_order(order_id)
        
    