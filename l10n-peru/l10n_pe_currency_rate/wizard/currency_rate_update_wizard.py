# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
import requests

class PeCurrencyRateUpdateWizard(models.TransientModel):
    
    _name = "pe.currency.rate.update.wizard"
    
    rate_update_id = fields.Many2one("pe.currency.rate.update.service", "Rate update", 
                                     default = lambda self: self.env.context.get('active_ids',[]) and self.env.context.get('active_ids',[])[0] or False)
    start_date = fields.Date("Start date", required = True)
    end_date = fields.Date("End date", required = True)
    
    @api.one
    def get_currency_rate(self):
        start_date = fields.Date.from_string(self.start_date)
        end_date = fields.Date.from_string(self.end_date)
        days = end_date - start_date
        if days.days < 0:
            raise ValidationError(_('The end date must be greater than the start date'))
        
        while days.days >= 0:
            day = days.days
            self.with_context(date = fields.Date.to_string(end_date)).rate_update_id.refresh_currency()
            day -=1
            end_date = start_date + timedelta(day)
            days = end_date - start_date