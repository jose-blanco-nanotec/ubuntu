��    8      �  O   �      �     �     �  !   �  5        N  	   U     _  
   g  
   r     }  &   �     �     �     �     �     �  
        #     *     7  	   @     J  �   M  T   �     :     W     m          �     �     �     �     �     �     �     �          /     8     G     Z     f  
   k  0   v  2   �     �  #   �     	  
   #	      .	     O	     a	  1   i	     �	     �	    �	     �
     �
  %     =   ;     y  
   �  	   �  
   �  	   �     �  )   �     �     �  #      &   $  %   K     q     �     �     �  
   �     �  �   �  W   U  %   �     �     �                4  	   J     T     c          �     �     �     �     �     �     �       
   
  0     =   F     �  /   �     �  
   �      �            1        Q     p                  
   .                /   7   5          1           4       -                	       *       "                    2       6                           )   &   $       !                       3                ,         '           (   +       8   %                 #   0    Add Rate Auto-download Rates Automatic Currency Rates Download Automatic download of currency rates for this company Cancel Companies Company Created by Created on Currencies available Currencies to update with this service Currency Currency Rate Currency Rate Update Currency update frequency Currency update services Date Range Day(s) Display Name End date Frequency ID If the time delta between the rate date given by the webservice and the current date exceeds this value, then the currency rate is not updated in Odoo. In company '%s', the rate of the main currency (%s) must be 1.00 (current rate: %s). Interval number must be >= 0 Inverse rate purchase Inverse rate sale Last Modified on Last Updated by Last Updated on Logs Max Delta Days Max delta days must be >= 0 Month(s) Next run on Peruvian Currency Rate Update Peruvian Rate Auto-download Purchase Rate for Range Rate in Date Range Rate update Sale Start date The end date must be greater than the start date The rate of the currency to the currency of rate 1 Unable to import urllib. Update exchange rates automatically Update logs Update now Web Service does not exist (%s)! Webservice to use Week(s) You can use a service only one time per company ! pe.currency.rate.update.wizard res.config.settings Project-Id-Version: Odoo Server 11.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-03-30 00:37-0500
Last-Translator: <>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
Language: es_PE
X-Generator: Poedit 2.0.6
 Agregar tasa Tasas de descarga automática Tasas de cambio automático Descargar Descarga automática de las tasas de cambio para esta empresa Cancelar Compañias Compañia Creado por Creado en Monedas disponibles Monedas para actualizar con este servicio Moneda Tasa Monetaria Actualización de la tasa de cambio Frecuencia de actualización de moneda Servicios de actualización de moneda Rango de fechas Día(s) Nombre a Mostrar Fecha final Frecuencia ID Si el delta de tiempo entre la fecha de tarifa dada por el servicio web y la fecha actual excede este valor, entonces la tasa de cambio no se actualiza en Odoo. En la empresa '%s', la tasa de la moneda principal (%s) debe ser 1.00 (tasa actual:%s). El número de intervalo debe ser> = 0 Tasa de compra inversa Tasa de venta inversa Ultima Modificación en Actualizado última vez por Ultima Actualización Registros Max Delta Days Max delta days must be >= 0 Month(s) Next run on Peruvian Currency Rate Update Peruvian Rate Auto-download Purchase Rate for Range Rate in Date Range Rate update Sale Start date The end date must be greater than the start date La tasa de cambio de la moneda respecto a la moneda de tasa 1 Unable to import urllib. Actualizar las tasas de cambio automáticamente Update logs Update now Web Service does not exist (%s)! Webservice to use Week(s) You can use a service only one time per company ! pe.currency.rate.update.wizard res.config.settings 