from odoo import models, fields, api, _
from odoo.tools import float_compare

class Currency(models.Model):
    _inherit = "res.currency"
    
    rate = fields.Float(compute='_compute_current_rate', string='Current Rate', digits=(12, 16),
                        help='The rate of the currency to the currency of rate 1.')
    rate_purchase = fields.Float("Purchase", compute='_compute_peruvian_current_rate', digits=(12, 16), help='The rate of the currency to the currency of rate 1')
    rate_sale = fields.Float("Sale", compute='_compute_peruvian_current_rate', digits=(12, 16), help='The rate of the currency to the currency of rate 1')
    inverse_rate_purchase = fields.Float("Inverse rate purchase", compute='_compute_peruvian_current_rate', 
                                         digits=(12, 6), help='The rate of the currency to the currency of rate 1')
    inverse_rate_sale = fields.Float("Inverse rate sale", compute='_compute_peruvian_current_rate', 
                                     digits=(12, 6), help='The rate of the currency to the currency of rate 1')
    
    @api.multi
    def _compute_current_rate(self):
        super(Currency, self)._compute_current_rate()
        
    @api.multi
    @api.depends('rate')
    def _compute_peruvian_current_rate(self):
        date = self._context.get('date') or fields.Date.today()
        company_id = self._context.get('company_id') or self.env['res.users']._get_company().id
        for currency in self:
            rate = self.env['res.currency.rate'].search([('currency_id','=',currency.id), ('company_id','=',company_id), ('name', '<=',date)], 
                                                 order='company_id, name DESC', limit=1)
            currency.rate_purchase = rate and rate.rate_purchase or (float_compare(currency.rate,1.0, precision_digits=16)==1 and 1.0 or 0.0)
            currency.rate_sale = rate and rate.rate_sale or (float_compare(currency.rate,1.0, precision_digits=16)==1 and 1.0 or 0.0)
            currency.inverse_rate_purchase = rate and rate.rate_purchase and 1/rate.rate_purchase or (float_compare(currency.rate,1.0, precision_digits=16)==1 and 1.0 or 0.0)
            currency.inverse_rate_sale = rate and rate.rate_sale and 1/rate.rate_sale or (float_compare(currency.rate,1.0, precision_digits=16)==1 and 1.0 or 0.0)
    
    @api.model
    def _get_conversion_rate(self, from_currency, to_currency):
        #from_currency = from_currency.with_env(self.env)
        #to_currency = to_currency.with_env(self.env)
        #if self.env.context.get('type') in ['out_refund', 'out_invoice']:
        #    return to_currency.rate_sale / (from_currency.rate_sale or 1)
        #elif self.env.context.get('type') in ['in_invoice', 'in_refund']:
        #    return (to_currency.rate_purchase or to_currency.rate_purchase) / ((from_currency.rate_purchase or from_currency.rate_sale) or 1)
        #else:
        #    return super(Currency, self)._get_conversion_rate(from_currency, to_currency)
        return super(Currency, self)._get_conversion_rate(from_currency, to_currency)
            

class CurrencyRate(models.Model):
    _inherit = "res.currency.rate"

    rate = fields.Float(digits=(12, 16), help='The rate of the currency to the currency of rate 1')
    rate_purchase = fields.Float("Purchase", digits=(12, 16), help='The rate of the currency to the currency of rate 1')
    rate_sale = fields.Float("Sale", digits=(12, 16), help='The rate of the currency to the currency of rate 1')
    #inverse_rate_purchase = fields.Float("Purchase", digits=(12, 16), help='The rate of the currency to the currency of rate 1')
    #inverse_rate_sale = fields.Float("Sale", digits=(12, 16), help='The rate of the currency to the currency of rate 1')
