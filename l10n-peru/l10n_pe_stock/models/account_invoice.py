# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
import odoo.addons.decimal_precision as dp
from datetime import datetime

class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    pe_stock_ids = fields.Many2many(comodel_name="stock.picking", string="Pickings", 
                                    compute ="_compute_pe_stock_ids", readonly=True, copy=False)
    pe_stock_name = fields.Char("Picking Number", compute ="_compute_pe_stock_ids", copy=False)

    @api.multi
    def _compute_pe_stock_ids(self):
        for invoice in self:
            invoice_id = invoice.debit_invoice_id or invoice.refund_invoice_id or invoice
            picking_ids =  invoice_id.invoice_line_ids.mapped('sale_line_ids').mapped('order_id').mapped('picking_ids') #self._cr.fetchall()
            pe_stock_ids = []
            numbers = []
            for picking_id in picking_ids:
                if picking_id.state not in ['draft', 'cancel']: 
                    numbers.append(picking_id.name)
                    pe_stock_ids.append(picking_id.id)
            if numbers:
                if len(numbers)==1:
                    invoice_id.pe_stock_name=numbers[0]
                else:
                    invoice_id.pe_stock_name = " - ".join(numbers)
            invoice_id.pe_stock_ids = pe_stock_ids
    
class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    pack_lot_ids = fields.Many2many(comodel_name="stock.move.line", string="Lots/Serials", 
                                    compute ="_get_pack_lot_ids", readonly=True, copy=False)
    pack_lot_name = fields.Char("Lots/Serials Name", compute ="_get_pack_lot_ids")

    
    @api.multi
    @api.depends("invoice_id.pe_stock_ids", 'product_id')
    def _get_pack_lot_ids(self):
        for line in self:
            if line.invoice_id.pe_stock_ids.mapped('move_line_ids').mapped('lot_id'):
                pack_lot_ids = line.invoice_id.pe_stock_ids.mapped('move_line_ids').filtered(lambda lot: lot.product_id == line.product_id)
                pack_lot_name=[]
                for pack_lot_id in pack_lot_ids:
                    name=""
                    if line.product_id.tracking=='serial':
                        name+="S/N. "
                    elif line.product_id.tracking=='lot':
                        name+="Lt. "
                    name+= (pack_lot_id.lot_id and pack_lot_id.lot_id.name or pack_lot_id.lot_name or "")+" "
                    if pack_lot_id.product_qty:
                        name+= "Cant. %s" % str(pack_lot_id.product_qty)
                    if pack_lot_id.lot_id.life_date:
                        name+= "FV. %s" % datetime.strptime(pack_lot_id.lot_id.life_date,'%Y-%m-%d %H:%M:%S').date().strftime('%d/%m/%Y')
                    pack_lot_name.append(name)
                line.pack_lot_name= pack_lot_name and "\n".join(pack_lot_name) or False
                line.pack_lot_ids = pack_lot_ids.ids

    class SaleOrder(models.Model):
        _inherit = "sale.order"