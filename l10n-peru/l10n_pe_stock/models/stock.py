# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class Picking(models.Model):
    _inherit = "stock.picking"

    pe_invoice_ids = fields.Many2many(comodel_name="stock.picking", string="Pickings", 
                                    compute ="_compute_pe_invoice_ids", readonly=True)
    pe_invoice_name = fields.Char("Internal Number", compute ="_compute_pe_invoice_ids")
    
    pe_type_operation = fields.Selection("_get_pe_type_operation", "Tipo de operación", 
                                         help="Tipo de operación efectuada", copy=False)
    pe_number = fields.Char("Numero de Guia")
    
    @api.model
    def _get_pe_type_operation(self):
        return self.env['pe.datas'].get_selection("PE.TABLA12")    
    
    @api.multi
    def _compute_pe_invoice_ids(self):
        pe_invoice_ids = []
        pe_invoice_name=[]
        for stock_id in self:
            stock_id.sale_id
            for invoice_id in stock_id.sale_id.invoice_ids:
                pe_invoice_ids.append(invoice_id.id)
                if invoice_id.state not in ['draft', 'cancel', 'annul'] and invoice_id.number:
                    pe_invoice_name.append(invoice_id.number)
        self.pe_invoice_ids=pe_invoice_ids
        self.pe_invoice_name=" ".join(pe_invoice_name)