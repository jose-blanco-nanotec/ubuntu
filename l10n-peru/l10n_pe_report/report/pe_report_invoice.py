
from odoo import api, fields, models, _
from odoo.tools.misc import formatLang


class SaleReportReport(models.AbstractModel):
    _name = "report.l10n_pe_report.report_invoice_report"
    
    def _sum_total(self, invoice_ids, currency_id):
        currency_obj = self.env['res.currency'].browse(currency_id)
        amount_total = 0.0
        residual = 0.0
        amount_untaxed = 0.0
        amount_tax = 0.0
        for invoice_id in invoice_ids:
            amount_total += invoice_id.amount_total
            residual += invoice_id.residual
            amount_untaxed += invoice_id.amount_untaxed
            amount_tax += invoice_id.amount_tax
        format_amount_total = formatLang(invoice_ids.env, amount_total, currency_obj = currency_obj)
        format_residual = formatLang(invoice_ids.env, residual, currency_obj = currency_obj)
        format_amount_untaxed = formatLang(invoice_ids.env, amount_untaxed, currency_obj = currency_obj)
        format_amount_tax = formatLang(invoice_ids.env, amount_tax, currency_obj = currency_obj)
        return {'amount_total':format_amount_total,'residual':format_residual,'amount_untaxed':format_amount_untaxed,'amount_tax':format_amount_tax}

    #def _get_margin(self, line):
    #    if line.purchase_price:
    #        margin = line.margin*100/(line.purchase_price*line.product_uom_qty)
    #        return {'margin': margin, 'fmargin' : formatLang(line.env, margin, digits=2)}
    #    else:
    #        return {'margin': 100.0, 'fmargin' : formatLang(line.env, 100.0, digits=2)}
    
    @api.model
    def get_report_values(self, docids, data=None):
        invoice_repor = self.env['pe.invoice.report.wizard'].browse(data['invoice_repor_id'])
        invoice_ids = self.env['account.invoice'].browse((data['invoice_ids']))
        docs = invoice_ids
        data['currency_ids'] = invoice_ids.mapped('currency_id')
        data['invoice_ids'] = invoice_ids
        data['journal_ids'] = invoice_ids.mapped('journal_id')
        return {'docs': invoice_repor, 'data':data, 'invoice_report': self}
    