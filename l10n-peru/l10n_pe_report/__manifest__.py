# -*- coding: utf-8 -*-
{
    'name': "Invoice Report",

    'summary': """
        Invoice, Report""",

    'description': """
        Invoice Reports
    """,

    'author': "FLEXXOONE",
    'website': "http://www.flexxoone.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['sale_margin', 'account', 'l10n_pe_cpe'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'wizard/invoice_report_wizard_view.xml',
        'views/report_account_invoice.xml',
        'views/invoice_report_view.xml',
    ],
}