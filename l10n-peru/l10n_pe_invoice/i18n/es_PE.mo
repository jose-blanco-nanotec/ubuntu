��    B      ,  Y   <      �     �     �     �     �  	   �     �     �     �  
     
             %     .     7     L  
   ]     h     t     �     �     �     �     �     �     �     �     �  +   �     #     ,     =  "   E     h     y     �     �     �     �     �     �     �     �               '     /     @  	   M     W     g  �   �     k	     t	     x	  *   ~	     �	     �	     �	     �	     �	     �	     
     
     %
     ?
    ^
     z     �     �     �  
   �  
   �     �     �  
   �  	   �     �                    4     E     U     b     s     v     �     �     �     �     �     �     �  .        :     C     Y  -   `     �     �     �     �     �                    +     B     `     i     �     �     �  	   �     �     �    �            	     2        Q     W     s     �     �     �     �     �     �          (      0   >                  /                         B          	          )   ?       
   &         #   $                     1      =   :   <   9              -              .      ,   '       A      %                 3   4   !   2   5   *                 6   +                   "      8         7   ;   @              Account Amount Amount Discount Cancel Companies Company Create Debit Note Create related invoices Created by Created on Credit Note Currency Customer Customer Credit Note Customer Invoice Debit Note Description Display Name ID Internal Number Invoice Invoice Date Invoice Line Invoice Lines Invoice Type Invoice Type Code Invoice Type Refund Invoice for which this invoice is the debit Invoices Invoices Related Journal Keep empty to use the current date Last Modified on Last Updated by Last Updated on Multi Related Invoice Name Paid Amount Partner Peruvian Localization Peruvian Settings Product Unit of Measure Quantity Reference/Description Related Related Invoices Related Line Retention SUNAT Unit Code Sales Advance Payment Invoice Select 'Sale' for customer invoices journals.
Select 'Purchase' for vendor bills journals.
Select 'Cash' or 'Bank' for journals that are used in customer or vendor payments.
Select 'General' for miscellaneous operations journals. Subtotal Tax Taxes The partner account used for this invoice. Total Total amount with taxes Total amount without taxes Type Unit of Measure Untaxed Amount Vendor Bill Vendor Credit Note pe.related.invoice.wizard pe.related.invoice.wizard.line Project-Id-Version: Odoo Server 11.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-05-20 20:17-0500
Last-Translator: <>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
Language: es_PE
X-Generator: Poedit 2.0.6
 Cuenta Importe Moento de Descuento Cancelar Compañias Compañía Crear nota de Débito Create related invoices Creado por Creado el Nota de crédito Moneda Cliente Customer Credit Note Customer Invoice Nota de Débito Descripción Nombre a Mostrar ID Número Interno Factura Fecha de la factura Detalle de Factura Líneas de la factura Tipo de Factura Código del Documento Tipo de factura Reembolso Factura por la cual esta factura es el débito Facturas Facturas Relacionadas Diario Dejarlo vacío para utilizar la fecha actual. Última Modificación en Última Actualización por Última Actualización el Factura relacionada múltiple Nombre Monto de pago Socio Localización Peruana Configuración Peruana Unidad de Medida del Producto Cantidad Referencia/Descripción Relacionado Facturas Relaciondas Lineas Relacionadas Retention SUNAT Unit Code Ventas. Anticipo pago factura Seleccione 'Ventas' para diarios de facturas de cliente.
Seleccione 'Compras' para diarios de facturas de proveedor.
Seleccione 'Caja' o 'Banco' para diarios que se usan para pagos de clientes y proveedores. Seleccione 'General' para diarios que contienen operaciones varias. Subtotal Impuesto Impuestos La cuenta de asociado utilizada para esta factura. Total Importe total con impuestos Importe total sin impuestos Tipo Unidad de Medida Base imponible Factura de proveedor Nota de crédito del vendedor pe.related.invoice.wizard pe.related.invoice.wizard.line 