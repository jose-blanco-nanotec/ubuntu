# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
import odoo.addons.decimal_precision as dp
from datetime import datetime

class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    internal_number = fields.Char("Internal Number", readonly=True, copy=False)
    debit_invoice_id = fields.Many2one('account.invoice', string="Invoice for which this invoice is the debit")
    pe_related_ids = fields.Many2many("account.invoice", string="Related Invoices", compute="_get_related_ids")
    
    @api.multi
    def get_pe_data_by_code(self, table_code, code):
        self.ensure_one()
        return self.env['pe.datas'].search([('table_code','=',table_code),('code', '=', code)], limit=1)
    
    @api.multi
    @api.depends('invoice_line_ids')
    def _get_related_ids(self):
        for invoice_id in self:
            related_ids = invoice_id.invoice_line_ids.mapped('pe_invoice_id').ids or []
            #if invoice_id.debit_invoice_id:
            #    related_ids.append(invoice_id.debit_invoice_id.id)
            if invoice_id.refund_invoice_id:
                related_ids.append(invoice_id.refund_invoice_id.id)
            invoice_id.pe_related_ids = related_ids
    
    @api.model
    def _prepare_refund(self, invoice, date_invoice=None, date=None, description=None, journal_id=None):
        res = super(AccountInvoice, self)._prepare_refund(invoice, date_invoice=date_invoice, date=date, 
                                                          description=description, journal_id=journal_id)
        journal_id= res.get('journal_id')
        if journal_id and not self.env.context.get("is_pe_debit_note"):
            journal = self.env['account.journal'].browse(journal_id)
            res['journal_id'] =  journal.credit_note_id and journal.credit_note_id.id or journal.id  
        elif journal_id and self.env.context.get("is_pe_debit_note"):
            journal = self.env['account.journal'].browse(journal_id)
            res['journal_id'] =  journal.dedit_note_id and journal.dedit_note_id.id or journal.id  
            res['type']="out_invoice"
            #res['debit_invoice_id']=invoice.id
            res['refund_invoice_id']=invoice.id
        return res

    @api.multi
    def invoice_validate(self):
        res= super(AccountInvoice,self).invoice_validate()
        for invoice_id in self:
            invoice_id.internal_number= invoice_id.number  
        return res
    
class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    amount_discount = fields.Float("Amount Discount", compute = "_compute_amount_discount")
    pe_invoice_ids = fields.Many2many('account.invoice', 'pe_account_invoice_line_invoice_rel', 'line_id', 'invoice_id', string="Invoices", copy=False, readonly=True)
    pe_invoice_id = fields.Many2one('account.invoice', string="Invoices", copy=False, readonly=True)
    #pe_amount_discount_included = fields.Float("Amount Discount Included", compute = "_compute_amount_discount")
    
    
    @api.multi
    @api.depends('price_unit', 'discount', 'invoice_id.currency_id')
    def _compute_amount_discount(self):
        for line in self:
            price = line.price_unit * (line.discount or 0.0) / 100.0
            amount_discount = line.invoice_line_tax_ids.compute_all(price, line.invoice_id.currency_id, 
                                                                    line.quantity, line.product_id, line.invoice_id.partner_id)
            line.amount_discount = amount_discount['total_excluded']
            #line.pe_amount_discount_included = amount_discount['total_included']
    
    def set_pe_affectation_code(self):
        return True