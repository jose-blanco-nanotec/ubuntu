# -*- coding: utf-8 -*-

from . import account
from . import account_invoice
from . import product
from . import res_company
from . import currency