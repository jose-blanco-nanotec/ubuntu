# -*- coding: utf-8 -*-

import datetime
import time
from dateutil.relativedelta import relativedelta
from datetime import date

from odoo import fields, models, api, _

class StockMovePrint(models.TransientModel):
    _name = 'stock.move.print'
    
    today = date.today()
    first_day = today.replace(day=1)
    last_day = datetime.datetime(today.year,today.month,1)+relativedelta(months=1,days=-1)
    
    product_id = fields.Many2one(
        'product.product',
        string='Product',
        required=True,
    )
    start_date = fields.Date(
        string='From Date',
        required=True,
        default=first_day,
    )
    end_date = fields.Date(
        string='To Date',
        required=True,
        default=last_day,
    )
    location_id = fields.Many2one(
        'stock.location',
        string='Source Location'
    )
    location_dest_id = fields.Many2one(
        'stock.location',
        string='Destination Location'
    )
    company_id = fields.Many2one(
        'res.company',
        string='Company',
        default=lambda self: self.env.user.company_id,
        required=True,
    )
    
    @api.multi
    def print_stock_move(self):
        data = self.read()[0]
        domain = []
        domain.append(('date', '>=', self.start_date))
        domain.append(('date', '<=', self.end_date))
        domain.append(('product_id', '=', self.product_id.id))

        if self.location_id:
            domain.append(('location_id', '=', self.location_id.id))
        if self.location_dest_id:
            domain.append(('location_dest_id', '=', self.location_dest_id.id))
        if self.company_id:
            domain.append(('company_id', '=', self.company_id.id))
        
        stock_moves_ids = self.env['stock.move'].search(domain, order='date ASC').ids
        datas = {'ids': stock_moves_ids}
        datas.update(model='stock.move')
        datas.update({'form': data})
        #act = self.env['report'].get_action(self, 'print_stock_move.stock_move_report_id', data=datas)
        return self.env.ref('print_stock_move.stock_moves_report').report_action(self, data=datas, config=False)   # odoo 11
        #return act
