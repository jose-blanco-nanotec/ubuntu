# -*- coding: utf-8 -*-

import time
from odoo import api, fields, models

class ReportStockMove(models.AbstractModel):
    _name = 'report.print_stock_move.stock_move_report_id'

    @api.model
    #def render_html(self, docids, data=None):
    def get_report_values(self, docids, data=None):   # odoo 11
        stock_moves_ids = data['ids']
        report = self.env['ir.actions.report']._get_report_from_name('print_stock_move.stock_move_report_id')
        #docargs = {
        return {        # odoo 11
            'doc_ids': stock_moves_ids,
            'doc_model': 'stock.move',
            'docs': self.env['stock.move'].browse(stock_moves_ids),
            'data':data,
        }
        #return self.env['report'].render('print_stock_move.stock_move_report_id', values=docargs)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
