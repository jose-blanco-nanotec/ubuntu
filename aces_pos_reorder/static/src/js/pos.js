odoo.define('aces_pos_reorder.pos', function (require) {
	"use strict";
	var gui = require('point_of_sale.gui');
	var models = require('point_of_sale.models');
	var screens = require('point_of_sale.screens');
	var core = require('web.core');
	var DB = require('point_of_sale.DB');
	var keyboard = require('point_of_sale.keyboard').OnscreenKeyboardWidget;
	var rpc = require('web.rpc');
	var utils = require('web.utils');
	var PopupWidget = require('point_of_sale.popups');

	var QWeb = core.qweb;
	var round_pr = utils.round_precision;
	
	var ShowOrderList = screens.ActionButtonWidget.extend({
	    template : 'ShowOrderList',
	    button_click : function() {
	        self = this;
	        self.gui.show_screen('orderlist');
	    },
	});

	screens.define_action_button({
	    'name' : 'showorderlist',
	    'widget' : ShowOrderList,
	    'condition': function(){
	    	return this.pos.config.enable_reorder
	    },
	});
	
	var SaveDraftButton = screens.ActionButtonWidget.extend({
	    template : 'SaveDraftButton',
	    button_click : function() {
	        var self = this;
            var selectedOrder = this.pos.get_order();
            selectedOrder.initialize_validation_date();
            var currentOrderLines = selectedOrder.get_orderlines();
            var orderLines = [];
            _.each(currentOrderLines,function(item) {
                return orderLines.push(item.export_as_JSON());
            });
            if (orderLines.length === 0) {
                return alert ('Please select product !');
            } else {
            	if( this.pos.config.require_customer && !selectedOrder.get_client()){
            		self.gui.show_popup('error',{
                        message: _t('An anonymous order cannot be confirmed'),
                        comment: _t('Please select a client for this order. This can be done by clicking the order tab')
                    });
                    return;
            	}
                this.pos.push_order(selectedOrder);
                self.gui.show_screen('receipt');
            }
        
	    },
	});

	screens.define_action_button({
	    'name' : 'savedraftbutton',
	    'widget' : SaveDraftButton,
	    'condition': function(){
	    	return this.pos.config.enable_reorder
	    },
	});

	/* Order list screen */
	var OrderListScreenWidget = screens.ScreenWidget.extend({
	    template: 'OrderListScreenWidget',

	    init: function(parent, options){
	    	var self = this;
	        this._super(parent, options);
	        this.reload_btn = function(){
	        	$('.fa-refresh').toggleClass('rotate', 'rotate-reset');
	        	self.reloading_orders();
	        };
	        if(this.pos.config.iface_vkeyboard && self.chrome.widget.keyboard){
            	self.chrome.widget.keyboard.connect(this.$('.searchbox input'));
            }
	    },
	    events: {
	    	'click .button.back':  'click_back',
	    	'keyup .searchbox input': 'search_order',
	    	'click .searchbox .search-clear': 'clear_search',
	        'click .button.draft':  'click_draft',
	        'click .button.paid': 'click_paid',
	        'click .button.posted': 'click_posted',
	        'click #print_order': 'click_reprint',
	        'click #view_lines': 'click_view_lines',
	        'click #edit_order': 'click_edit_or_duplicate_order',
	        'click #re_order_duplicate': 'click_edit_or_duplicate_order',
	    },
	    filter:"all",
        date: "all",
        get_orders: function(){
        	return this.pos.get('pos_order_list');
        },
        click_back: function(){
        	this.gui.back();
        },
        click_draft: function(event){
        	var self = this;
        	if($(event.currentTarget).hasClass('selected')){
        		$(event.currentTarget).removeClass('selected');
        		self.filter = "all";
    		}else{
        		self.$('.button.paid').removeClass('selected');
        		self.$('.button.posted').removeClass('selected');
    			$(event.currentTarget).addClass('selected');
        		self.filter = "draft";
    		}
    		self.render_list(self.get_orders());
        },
        click_paid: function(event){
        	var self = this;
        	if($(event.currentTarget).hasClass('selected')){
        		$(event.currentTarget).removeClass('selected');
        		self.filter = "all";
    		}else{
        		self.$('.button.draft').removeClass('selected');
        		self.$('.button.posted').removeClass('selected');
        		$(event.currentTarget).addClass('selected');
        		self.filter = "paid";
    		}
        	self.render_list(self.get_orders());
        },
        click_posted: function(event){
        	var self = this;
        	if($(event.currentTarget).hasClass('selected')){
        		$(event.currentTarget).removeClass('selected');
        		self.filter = "all";
    		}else{
    			self.$('.button.paid').removeClass('selected');
    			self.$('.button.draft').removeClass('selected');
    			$(event.currentTarget).addClass('selected');
        		self.filter = "done";
    		}
        	self.render_list(self.get_orders());
        },
        clear_cart: function(){
        	var self = this;
        	var order = this.pos.get_order();
        	var currentOrderLines = order.get_orderlines();
        	if(currentOrderLines && currentOrderLines.length > 0){
        		_.each(currentOrderLines,function(item) {
        			order.remove_orderline(item);
                });
        	} else {
        		return
        	}
        },
        
        show: function(){
        	var self = this;
	        this._super();
	        this.reload_orders();
	        $('input#datepicker').datepicker({
           	    dateFormat: 'yy-mm-dd',
                autoclose: true,
                closeText: 'Clear',
                showButtonPanel: true,
                onSelect: function (dateText, inst) {
                	var date = $(this).val();
					if (date){
					    self.date = date;
					    self.render_list(self.get_orders());
					}
				},
				onClose: function(dateText, inst){
                    if( !dateText ){
                        self.date = "all";
                        self.render_list(self.get_orders());
                    }
                }
           }).focus(function(){
                var thisCalendar = $(this);
                $('.ui-datepicker-close').click(function() {
                    thisCalendar.val('');
                    self.date = "all";
                    self.render_list(self.get_orders());
                });
           });
	    },
	    get_journal_from_order: function(statement_ids){
	    	var self = this;
	    	var order = this.pos.get_order();
	    	var params = {
	    		model: 'account.bank.statement.line',
	    		method: 'search_read',
	    		domain: [['id', 'in', statement_ids]],
	    	}
	    	rpc.query(params, {async: false}).then(function(statements){
	    		if(statements.length > 0){
	    			var order_statements = []
	    			_.each(statements, function(statement){
	    				if(statement.amount > 0){
	    					order_statements.push({
	    						amount: statement.amount,
	    						journal: statement.journal_id[1],
	    					})
	    				}
	    			});
	    			order.set_journal(order_statements);
	    		}
	    	});
	    },
	    get_orderlines_from_order: function(line_ids){
	    	var self = this;
	    	var order = this.pos.get_order();
	    	var orderlines = false;
	    	var params = {
	    		model: 'pos.order.line',
	    		method: 'search_read',
	    		domain: [['id', 'in', line_ids]],
	    	}
	    	rpc.query(params, {async: false}).then(function(order_lines){
	    		if(order_lines.length > 0){
	    			orderlines = order_lines;
	    		}
	    	});
	    	return orderlines
	    },
	    click_reprint: function(event){
        	var self = this;
        	var selectedOrder = this.pos.get_order();
        	var order_id = parseInt($(event.currentTarget).data('id'));
        	
        	self.clear_cart();
        	selectedOrder.set_client(null);
        	var result = self.pos.db.get_order_by_id(order_id);
        	if (result && result.lines.length > 0) {
        		if (result.partner_id && result.partner_id[0]) {
                    var partner = self.pos.db.get_partner_by_id(result.partner_id[0])
                    if(partner){
                    	selectedOrder.set_client(partner);
                    }
                }
        		selectedOrder.set_amount_paid(result.amount_paid);
                selectedOrder.set_amount_return(Math.abs(result.amount_return));
                selectedOrder.set_amount_tax(result.amount_tax);
                selectedOrder.set_amount_total(result.amount_total);
                selectedOrder.set_company_id(result.company_id[1]);
                selectedOrder.set_date_order(result.date_order);
                selectedOrder.set_pos_reference(result.pos_reference);
                selectedOrder.set_user_name(result.user_id && result.user_id[1]);
                if(result.statement_ids.length > 0){
                	self.get_journal_from_order(result.statement_ids);
                }
                if(result.lines.length > 0){
                	var order_lines = self.get_orderlines_from_order(result.lines);
                	if(order_lines.length > 0){
	                	_.each(order_lines, function(line){
		    				var product = self.pos.db.get_product_by_id(Number(line.product_id[0]));
		    				if(product){
		    					selectedOrder.add_product(product, {
		    						quantity: line.qty,
		    						discount: line.discount,
		    						price: line.price_unit,
		    					})
		    				}
		    			})
                	}
                }
                selectedOrder.set_order_id(order_id);
                self.gui.show_screen('receipt');
        	}
        },
        click_view_lines: function(event){
        	var self = this;
        	var order_id = parseInt($(event.currentTarget).data('id'));
            var order = this.pos.get_order();
            var result = self.pos.db.get_order_by_id(order_id);
            if(result.lines.length > 0){
            	var order_lines = self.get_orderlines_from_order(result.lines);
            	if(order_lines){
            		self.gui.show_popup('product_popup', {
            			order_lines: order_lines,
            			order_id: order_id,
            			state: result.state,
            			order_screen_obj: self,
            		});
            	}
            }
        },
        click_edit_or_duplicate_order: function(event){
        	var self = this;
        	var order_id = parseInt($(event.currentTarget).data('id'));
            var selectedOrder = this.pos.get_order();
            var result = self.pos.db.get_order_by_id(order_id);
            if(result.lines.length > 0){
            	if($(event.currentTarget).data('operation') === "edit"){
	            	if(result.state == "paid"){
	                	alert("Sorry, This order is paid State");
	                	return
	                }
	                if(result.state == "done"){
	                	alert("Sorry, This Order is Done State");
	                	return
	                }
            	}
                self.clear_cart();
            	selectedOrder.set_client(null);
            	if (result.partner_id && result.partner_id[0]) {
                    var partner = self.pos.db.get_partner_by_id(result.partner_id[0])
                    if(partner){
                    	selectedOrder.set_client(partner);
                    }
                }
            	if($(event.currentTarget).data('operation') !== "reorder"){
	           	 	selectedOrder.set_pos_reference(result.pos_reference);
	           	 	selectedOrder.set_order_id(order_id);
	           	 	selectedOrder.set_sequence(result.name);
            	}
	           	if(result.lines.length > 0){
	            	var order_lines = self.get_orderlines_from_order(result.lines);
	            	if(order_lines.length > 0){
		               	_.each(order_lines, function(line){
			    			var product = self.pos.db.get_product_by_id(Number(line.product_id[0]));
			    			if(product){
			    				selectedOrder.add_product(product, {
			    					quantity: line.qty,
			    					discount: line.discount,
			    					price: line.price_unit,
			    				})
			    			}
			    		})
	            	}
	            }
	           	self.gui.show_screen('products');
            }
        },
	    search_order: function(event){
	    	var self = this;
	    	var search_timeout = null;
	    	clearTimeout(search_timeout);
            var query = $(event.currentTarget).val();
            search_timeout = setTimeout(function(){
                self.perform_search(query,event.which === 13);
            },70);
	    },
	    perform_search: function(query, associate_result){
            if(query){
                var orders = this.pos.db.search_order(query);
                if ( associate_result && orders.length === 1){
                    this.gui.back();
                }
                this.render_list(orders);
            }else{
                var orders = self.pos.get('pos_order_list');
                this.render_list(orders);
            }
        },
        clear_search: function(){
            var orders = this.pos.get('pos_order_list');
            this.render_list(orders);
            this.$('.searchbox input')[0].value = '';
            this.$('.searchbox input').focus();
        },
        check_filters: function(orders){
        	var self = this;
        	var filtered_orders = false;
        	if(self.filter !== "" && self.filter !== "all"){
	            filtered_orders = $.grep(orders,function(order){
	            	return order.state === self.filter;
	            });
            }
        	return filtered_orders || orders;
        },
        check_date_filter: function(orders){
        	var self = this;
        	var date_filtered_orders = [];
        	if(self.date !== "" && self.date !== "all"){
            	
            	for (var i=0; i<orders.length;i++){
                    var date_order = $.datepicker.formatDate("yy-mm-dd",new Date(orders[i].date_order));
            		if(self.date === date_order){
            			date_filtered_orders.push(orders[i]);
            		}
            	}
            }
        	return date_filtered_orders;
        },
	    render_list: function(orders){
        	var self = this;
        	if(orders){
	            var contents = this.$el[0].querySelector('.order-list-contents');
	            contents.innerHTML = "";
	            var temp = [];
	            orders = self.check_filters(orders);
	            if(self.date !== "" && self.date !== "all"){
	            	orders = self.check_date_filter(orders);
	            }
	            for(var i = 0, len = Math.min(orders.length,1000); i < len; i++){
	                var order    = orders[i];
	                order.amount_total = parseFloat(order.amount_total).toFixed(2); 
	            	var clientline_html = QWeb.render('OrderlistLine',{widget: this, order:order});
	                var clientline = document.createElement('tbody');
	                clientline.innerHTML = clientline_html;
	                clientline = clientline.childNodes[1];
	                contents.appendChild(clientline);
	            }
	            $("table.order-list").simplePagination({
					previousButtonClass: "btn btn-danger",
					nextButtonClass: "btn btn-danger",
					previousButtonText: '<i class="fa fa-angle-left fa-lg"></i>',
					nextButtonText: '<i class="fa fa-angle-right fa-lg"></i>',
					perPage:self.pos.config.record_per_page > 0 ? self.pos.config.record_per_page : 10
				});
        	}
        },
        reload_orders: function(){
        	var self = this;
            var orders=self.pos.get('pos_order_list');
            this.render_list(orders);
        },
	    reloading_orders: function(){
	    	var self = this;
	    	var date = new Date();
	    	var params = {
				model: 'pos.order',
				method: 'ac_pos_search_read',
				args: [{'domain': this.pos.domain_as_args}],
			}
			return rpc.query(params, {async: false}).then(function(orders){
                if(orders.length > 0){
                	self.pos.db.add_orders(orders);
                    self.pos.set({'pos_order_list' : orders});
                    self.reload_orders();
                }
            }).fail(function (type, error){
                if(error.code === 200 ){    // Business Logic Error, not a connection problem
                   self.gui.show_popup('error-traceback',{
                        'title': error.data.message,
                        'body':  error.data.debug
                   });
                }
            });
	    },
	    renderElement: function(){
	    	var self = this;
	    	self._super();
	    	self.el.querySelector('.button.reload').addEventListener('click',this.reload_btn);
	    },
	});
	gui.define_screen({name:'orderlist', widget: OrderListScreenWidget});

	var _super_Order = models.Order.prototype;
    models.Order = models.Order.extend({
    	generateUniqueId_barcode: function() {
            return new Date().getTime();
        },
        generate_unique_id: function() {
            var timestamp = new Date().getTime();
            return Number(timestamp.toString().slice(-10));
        },
        // Order History
        set_sequence:function(sequence){
        	this.set('sequence',sequence);
        },
        get_sequence:function(){
        	return this.get('sequence');
        },
        set_order_id: function(order_id){
            this.set('order_id', order_id);
        },
        get_order_id: function(){
            return this.get('order_id');
        },
        set_amount_paid: function(amount_paid) {
            this.set('amount_paid', amount_paid);
        },
        get_amount_paid: function() {
            return this.get('amount_paid');
        },
        set_amount_return: function(amount_return) {
            this.set('amount_return', amount_return);
        },
        get_amount_return: function() {
            return this.get('amount_return');
        },
        set_amount_tax: function(amount_tax) {
            this.set('amount_tax', amount_tax);
        },
        get_amount_tax: function() {
            return this.get('amount_tax');
        },
        set_amount_total: function(amount_total) {
            this.set('amount_total', amount_total);
        },
        get_amount_total: function() {
            return this.get('amount_total');
        },
        set_company_id: function(company_id) {
            this.set('company_id', company_id);
        },
        get_company_id: function() {
            return this.get('company_id');
        },
        set_date_order: function(date_order) {
            this.set('date_order', date_order);
        },
        get_date_order: function() {
            return this.get('date_order');
        },
        set_pos_reference: function(pos_reference) {
            this.set('pos_reference', pos_reference)
        },
        get_pos_reference: function() {
            return this.get('pos_reference')
        },
        set_user_name: function(user_id) {
            this.set('user_id', user_id);
        },
        get_user_name: function() {
            return this.get('user_id');
        },
        set_journal: function(statement_ids) {
            this.set('statement_ids', statement_ids)
        },
        get_journal: function() {
            return this.get('statement_ids');
        },
        get_change: function(paymentline) {
			if(this.get_order_id()){
				if (!paymentline) {
		        	if(this.get_total_paid() > 0){
		        		var change = this.get_total_paid() - this.get_total_with_tax();
		        	}else{
		        		var change = this.get_amount_return();
		        	}
		        } else {
		            var change = -this.get_total_with_tax(); 
		            var lines  = this.pos.get_order().get_paymentlines();
		            for (var i = 0; i < lines.length; i++) {
		                change += lines[i].get_amount();
		                if (lines[i] === paymentline) {
		                    break;
		                }
		            }
		        }
		        return round_pr(Math.max(0,change), this.pos.currency.rounding);
			} else {
				return _super_Order.get_change.call(this, paymentline);
			}
        },
        export_as_JSON: function() {
        	var new_val = {};
            var orders = _super_Order.export_as_JSON.call(this);
            new_val = {
                old_order_id: this.get_order_id(),
                sequence: this.get_sequence(),
                pos_reference: this.get_pos_reference()
            }
            $.extend(orders, new_val);
            return orders;
        },
        export_for_printing: function(){
            var orders = _super_Order.export_for_printing.call(this);
            var new_val = {
            	reprint_payment: this.get_journal() || false,
            	ref: this.get_pos_reference() || false,
            	date_order: this.get_date_order() || false,
            };
            $.extend(orders, new_val);
            return orders;
        },
        set_date_order: function(val){
        	this.set('date_order',val)
        },
        get_date_order: function(){
        	return this.get('date_order')
        },
    });
    
	var _super_posmodel = models.PosModel;
	 models.PosModel = models.PosModel.extend({
		load_server_data: function(){
			var self = this;
			return _super_posmodel.prototype.load_server_data.call(this)
			.then(function(){
				if(self.config.enable_reorder){
					var from_date = moment().format('YYYY-MM-DD')
					if(self.config.last_days){
						from_date = moment().subtract(self.config.last_days, 'days').format('YYYY-MM-DD');
					}
					self.domain_as_args = [['state','not in',['cancel']], ['create_date', '>=', from_date]];
					var params = {
						model: 'pos.order',
						method: 'ac_pos_search_read',
						args: [{'domain': self.domain_as_args}],
					}
					rpc.query(params, {async: false}).then(function(orders){
		                if(orders.length > 0){
		                	self.db.add_orders(orders);
		                    self.set({'pos_order_list' : orders});
		                } else{
		                	self.set({'pos_order_list' : []});
		                }
	
		            });
				}
			});
		},
		_save_to_server: function (orders, options) {
			var self = this;
			return _super_posmodel.prototype._save_to_server.apply(this, arguments)
			.done(function(server_ids){
				if(server_ids.length > 0 && self.config.enable_reorder){
					var params = {
						model: 'pos.order',
						method: 'ac_pos_search_read',
						args: [{'domain': [['id','=',server_ids]]}],
					}
					rpc.query(params, {async: false}).then(function(orders){
		                if(orders.length > 0){
		                	orders = orders[0];
		                    var exist_order = _.findWhere(self.get('pos_order_list'), {'pos_reference': orders.pos_reference})
		                    if(exist_order){
		                    	_.extend(exist_order, orders);
		                    } else {
		                    	self.get('pos_order_list').push(orders);
		                    }
		                    var new_orders = _.sortBy(self.get('pos_order_list'), 'id').reverse();
		                    self.db.add_orders(new_orders);
		                    self.set({ 'pos_order_list' : new_orders });
		                }
		            });
				}
			});
		},
	});	
	
	DB.include({
		init: function(options){
        	this._super.apply(this, arguments);
        	this.group_products = [];
        	this.order_write_date = null;
        	this.order_by_id = {};
        	this.order_sorted = [];
        	this.order_search_string = "";
        },
        add_orders: function(orders){
            var updated_count = 0;
            var new_write_date = '';
            for(var i = 0, len = orders.length; i < len; i++){
                var order = orders[i];
                if (    this.order_write_date && 
                        this.order_by_id[order.id] &&
                        new Date(this.order_write_date).getTime() + 1000 >=
                        new Date(order.write_date).getTime() ) {
                    continue;
                } else if ( new_write_date < order.write_date ) { 
                    new_write_date  = order.write_date;
                }
                if (!this.order_by_id[order.id]) {
                    this.order_sorted.push(order.id);
                }
                this.order_by_id[order.id] = order;
                updated_count += 1;
            }
            this.order_write_date = new_write_date || this.order_write_date;
            if (updated_count) {
                // If there were updates, we need to completely 
                this.order_search_string = "";
                for (var id in this.order_by_id) {
                    var order = this.order_by_id[id];
                    this.order_search_string += this._order_search_string(order);
                }
            }
            return updated_count;
        },
        _order_search_string: function(order){
            var str =  order.name;
            if(order.pos_reference){
                str += '|' + order.pos_reference;
            }
            if(order.partner_id.length > 0){
                str += '|' + order.partner_id[1];
            }
            str = '' + order.id + ':' + str.replace(':','') + '\n';
            return str;
        },
        get_order_write_date: function(){
            return this.order_write_date;
        },
        get_order_by_id: function(id){
            return this.order_by_id[id];
        },
        search_order: function(query){
            try {
                query = query.replace(/[\[\]\(\)\+\*\?\.\-\!\&\^\$\|\~\_\{\}\:\,\\\/]/g,'.');
                query = query.replace(' ','.+');
                var re = RegExp("([0-9]+):.*?"+query,"gi");
            }catch(e){
                return [];
            }
            var results = [];
            var r;
            for(var i = 0; i < this.limit; i++){
                r = re.exec(this.order_search_string);
                if(r){
                    var id = Number(r[1]);
                    results.push(this.get_order_by_id(id));
                }else{
                    break;
                }
            }
            return results;
        },
	});
	/* Product POPUP */
	var ProductPopup = PopupWidget.extend({
	    template: 'ProductPopup',
	    show: function(options){
	    	var self = this;
			this._super();
			this.order_lines = options.order_lines || false;
			this.order_id = options.order_id || false;
			this.state = options.state || false;
			this.order_screen_obj = options.order_screen_obj || false;
			this.renderElement();
	    },
	    click_confirm: function(){
	        if (this.state == "paid" || this.state == "done"){
	        	$("#re_order_duplicate[data-id='"+ this.order_id +"']").click();
	        } else if(this.state == "draft") {
	        	$("#edit_order[data-id='"+ this.order_id +"']").click();
			}
			this.gui.close_popup();
	    },
    	click_cancel: function(){
    		this.gui.close_popup();
    	}
	    
	});
	gui.define_popup({name:'product_popup', widget: ProductPopup});
});
