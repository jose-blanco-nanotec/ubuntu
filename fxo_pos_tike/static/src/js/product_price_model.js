odoo.define('fxo_pos_tike.product_price_model', function (require) {
    "use strict";
    var screens = require('point_of_sale.screens');
    var models = require('point_of_sale.models');
    var PopUpWidget=require('point_of_sale.popups');
    var gui = require('point_of_sale.gui');
    var core = require('web.core');
    var QWeb = core.qweb;
    var _t = core._t;
    var datos; 


    models.load_fields("product.product",['class_fx']);
    models.load_fields("product.product",['model_fx']);
    models.load_fields("product.product",['style_fx']);
    var OrderSuper = models.Order;
    models.Order = models.Order.extend({
        
    get_item_model: function (data,class_fx_s,model_fx_s,style_fx_s) {
        var datos= [];
        var temp =[];
        var result =[];
        var id =0;
        var class_fx_p= class_fx_s;
        var model_fx_P= model_fx_s;
        var style_fx_P= style_fx_s;
        data.forEach(function(element) {

            console.log(element);
            var item  = 
                        {
                            'id'      : id,
                            'class_fx': element.get_product().class_fx,
                            'model_fx': element.get_product().model_fx,
                            'style_fx': element.get_product().style_fx,
                            'name'    : element.get_product().display_name,
                            'discount': element.get_discount(),
                            'cant'    : element.quantity,
                            'unit'    : element.get_unit().uom_id,
                            'total'   : element.get_display_price(),
                            'monto'   : element.get_unit_price()
                        };
            datos.push(item);
            id++;
        });
        datos.forEach(dato => {
            if( class_fx_p === true && model_fx_P === true && style_fx_P === true ){
                if (!temp.find(cat => cat.class_fx == dato.class_fx   &&  cat.model_fx == dato.model_fx &&  cat.style_fx == dato.style_fx   && cat.monto == dato.monto  )) {
            
                    const { id, class_fx,model_fx, style_fx ,name ,cant, total,monto} = dato;
        
                    temp.push({ id, name,class_fx,model_fx, style_fx ,cant, total,monto});
                }
            }else if(class_fx_p === true && model_fx_P === true && style_fx_P === false){
              
                if (!temp.find(cat => cat.class_fx == dato.class_fx   &&  cat.model_fx == dato.model_fx   && cat.monto == dato.monto  )) {
                
                    const { id, class_fx,model_fx, style_fx ,name ,cant, total,monto} = dato;
        
                    temp.push({ id, name,class_fx,model_fx, style_fx ,cant, total,monto});
                
              }   
            }else if(class_fx_p === true && model_fx_P === false && style_fx_P === false){
                 
                if (!temp.find(cat => cat.class_fx == dato.class_fx && cat.monto == dato.monto  )) {
        
                    const { id, class_fx,model_fx, style_fx ,name ,cant, total,monto} = dato;
        
                    temp.push({ id, name,class_fx,model_fx, style_fx ,cant, total,monto});
            }
            }else if(class_fx_p === false && model_fx_P === true && style_fx_P === true){
        
                if (!temp.find(cat =>  cat.model_fx == dato.model_fx &&  cat.style_fx == dato.style_fx   && cat.monto == dato.monto  )) {
            
                    const { id, class_fx,model_fx, style_fx ,name ,cant, total,monto} = dato;
        
                    temp.push({ id, name,class_fx,model_fx, style_fx ,cant, total,monto});
                }
            }else if(class_fx_p === false && model_fx_P === true && style_fx_P === false){
        
                if (!temp.find(cat =>  cat.model_fx == dato.model_fx   && cat.monto == dato.monto  )) {
            
                    const { id, class_fx,model_fx, style_fx ,name ,cant, total,monto} = dato;
        
                    temp.push({ id, name,class_fx,model_fx, style_fx ,cant, total,monto});
                }
            }else if(class_fx_p === true && model_fx_P === false && style_fx_P === true){
        
                if (!temp.find(cat =>  cat.class_fx == dato.class_fx   &&  cat.style_fx == dato.class_fx   && cat.monto == dato.monto  )) {
            
                    const { id, class_fx,model_fx, style_fx ,name ,cant, total,monto} = dato;
        
                    temp.push({ id, name,class_fx,model_fx, style_fx ,cant, total,monto});
                }
            }
        });
        
        temp.forEach(res => {
            datos.forEach(dato => {
                if(res.id != dato.id  ){
                    if( class_fx_p === true && model_fx_P === true && style_fx_P === true ){
                        if( res.class_fx == dato.class_fx && res.model_fx == dato.model_fx && res.style_fx == dato.style_fx   && res.monto == dato.monto  ){
                        res.total += dato.total;
                        res.cant  += dato.cant;
                    }
                  }else if(class_fx_p === true && model_fx_P === true && style_fx_P === false){
                    if( res.class_fx == dato.class_fx && res.model_fx == dato.model_fx && res.style_fx == dato.style_fx   && res.monto == dato.monto  ){
                        res.total += dato.total;
                        res.cant  += dato.cant;
                    }
                  }else if(class_fx_p === true && model_fx_P === false && style_fx_P === false){
                    if( res.class_fx == dato.class_fx  && res.monto == dato.monto  ){
                        res.total += dato.total;
                        res.cant  += dato.cant;
                    }
                  }else if(class_fx_p === false && model_fx_P === true && style_fx_P === true){
                    if( res.model_fx == dato.model_fx && res.style_fx == dato.style_fx && res.monto == dato.monto  ){
                        res.total += dato.total;
                        res.cant  += dato.cant;
                    }
                  }else if(class_fx_p === false && model_fx_P === true && style_fx_P === false){
                    if( res.model_fx == dato.model_fx && res.monto == dato.monto  ){
                        res.total += dato.total;
                        res.cant  += dato.cant;
                    } 
                  }
                }else if(class_fx_p === false && model_fx_P === false && style_fx_P === true){
                    if( res.style_fx == dato.style_fx && res.monto == dato.monto  ){
                        res.total += dato.total;
                        res.cant  += dato.cant;
                    } 
                }else if(class_fx_p === true && model_fx_P === false && style_fx_P === true){
                    if(res.class_fx == dato.class_fx && res.style_fx == dato.style_fx && res.monto == dato.monto  ){
                        res.total += dato.total;
                        res.cant  += dato.cant;
                    } 
                }
                        
            });
            if(res.model_fx !=false){ result.push(res)}
            
        });
        datos.forEach(dato => {
            if(dato.model_fx == false){ result.push(dato)}
        });
        
        console.log(temp);
        console.log(datos);
        console.log(result);
        return result;
    },

});


    
    
    });


 
