# -*- coding: utf-8 -*-
from odoo import api, fields, models, exceptions,tools, _

class pos_config_code(models.Model):
    _inherit = 'pos.config' 

    show_barcode = fields.Boolean('Mostrar Codigo de Barra', help='Monstrar codigo de barra en la Factura ',default=False)
    show_internal = fields.Boolean('Mostrar Referencia Interna  ', help='Monstrar Referencia interna en la Factura ' ,default=False)
    invoice_summary = fields.Boolean('Resumir Factura',help='Puede Resumir la Factura por Clase , Modelo y Factura  ' ,default=False)
    model_fx = fields.Boolean(' Modelo ',help='Resumir por modelo ', default=False)
    class_fx = fields.Boolean(' clase ', help='Resumir por Clase ', default=False)
    style_fx = fields.Boolean(' stylo ', help='Resumir por Stylo ', default=False)



    @api.constrains('invoice_summary')
    def check_pages(self):
        if self.invoice_summary == True:
            if (self.model_fx == True or self.class_fx == True) or  self.style_fx == True : 
                pass
            else :
                raise exceptions.ValidationError( "Activar almenos una de las siguiente opciones :Clase,Modelo y Stylo" )

    @api.onchange('invoice_summary')
    def check_invoice_summary(self):
        if self.invoice_summary == False:
            self.model_fx == False  
            self.class_fx == False  
            self.style_fx == False 